/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __sphere_hh
#define __sphere_hh

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <viewit/drawable.hh>

/**
   A class to store some values neccessary to describe a sphere;
 */

class CSphereDescr {
public: 
	/**
	   create a sphere description object
	   \param loc location of center of the sphere
	   \param radius radius of sphere
	   \param color of sphere
	 */ 
	CSphereDescr(const C3DFVector& loc, float radius, const TColor& color);
	
	/**
	   Preparing the drawing of the sphere by modifying the MODELVIEW-Matrix apropriately 
	   and by setting the right color.
	*/
	void gl_prep()const;
private:	
	C3DFVector m_loc; 
	float m_radius;
	TColor m_color;
};


class TSphereList: public TDrawable {
public:
	
	/** create the drawable object 
	    \param name the name of the object 
	*/
	TSphereList(const string& name);

	/** add a sphere to the list 
	    \param sphere the description of the sphere 
	*/
	void add_sphere(const CSphereDescr& sphere);

	/** allocate a glDrawList */
	virtual void gl_attach();
	
	/** free the glDrawList */
	virtual void gl_detach();
	
private: 
	virtual void do_gl_draw(const TCamera& c)const;
	virtual bool do_handle_key_event(int key);
	
	GLint m_GLSphereDrawList; 
	
	typedef std::list<CSphereDescr> SphereList; 
	SphereList m_list;
	GLUquadricObj *m_qobj;
};
#endif
