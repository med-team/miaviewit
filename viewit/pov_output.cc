/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <sstream>
#include <iomanip>

#include <viewit/pov_output.hh>

using namespace std; 
TPovOutput::TPovOutput(int snap_num)
{
	stringstream s; 
	s << "pov_snap" << setw(5) << setfill('0') << snap_num << ".pov"; 


	file = fopen(s.str().c_str(),"w");
	if (!file) 
		std::runtime_error("unable to create pov-output file");
}

TPovOutput::~TPovOutput()
{
}
	
void TPovOutput::init(const C3DFVector& trans, const C3DFVector& rot, const C3DFVector& shift,float width,float height)
{
	fprintf(file,"camera {\n\tlocation <0, 0, 0> ");
	fprintf(file,"\n\tdirection < 0, 0, -1.6666>");
	fprintf(file,"\n\tup < 0, 1, 0>");
	fprintf(file,"\n\tright < %f/%f, 0, 0>",width,height);
 	fprintf(file,"\n\ttranslate <%f, %f, %f >",-trans.x, -trans.y, -trans.z);
	fprintf(file,"\n\trotate <%f, 0, 0 >\n\trotate <0, %f, 0 >\n\trotate <0, 0, %f >",-rot.x, -rot.y, -rot.z);
	fprintf(file,"\n\ttranslate < %f, %f, %f >\n}\n",-shift.x, -shift.y, -shift.z);
	
	fprintf(file,"light_source {\n\t<128, 20, 256 >\n\tcolor rgb <1.0,1.0,1.0> \n");
	fprintf(file,"\n\ttranslate <%f, %f, %f >",-trans.x, -trans.y, -trans.z);
	fprintf(file,"\n\trotate <%f, 0, 0 >\n\trotate <0, %f, 0 >\n\trotate <0, 0, %f >",-rot.x, -rot.y, -rot.z);
	fprintf(file,"\n\ttranslate < %f, %f, %f >\n}\n",-shift.x, -shift.y, -shift.z);
	
	fprintf(file,"light_source {\n\t<-128, -220, 256 >\n\tcolor rgb <.3,.3,.3> \n");
	fprintf(file,"\n\ttranslate <%f, %f, %f >",-trans.x, -trans.y, -trans.z);
	fprintf(file,"\n\trotate <%f, 0, 0 >\n\trotate <0, %f, 0 >\n\trotate <0, 0, %f >",-rot.x, -rot.y, -rot.z);
	fprintf(file,"\n\ttranslate < %f, %f, %f >\n}\n",-shift.x, -shift.y, -shift.z);

	
	//	fprintf(file,"light_source {\n\t< -200128, -220, -156 >\n\tcolor rgb <1.0,1.0,1.0> \n}\n");
}
	
void TPovOutput::write(const TDrawMap& data)
{
	TDrawMap::const_iterator i = data.begin(); 
	while ( i != data.end() ) {
		if ((*i).second->is_visible()) {
			fprintf(file,"#declare %s = ",(*i).first.c_str());
			(*i).second->povray_output_attributes(file);
			fprintf(file,"%s\n",(*i).first.c_str());
		}
		i++;
	}
}
	
void TPovOutput::finish()
{
	
	fclose(file);
}

