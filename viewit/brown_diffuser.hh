/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __brow_diffuser_h
#define __brow_diffuser_h


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <mia.hh>
using namespace mia; 

#include <vector>

#include <viewit/drawable.hh>
#include <viewit/pixel.hh>

class TRandom {
public:	
	TRandom();
	float operator ()(float max)const;
	C3DFVector operator()(const C3DFVector& max)const;	
};


typedef std::vector<C3DFVector> TBrownVectors; 


TBrownVectors *brownies_init(int cnt, P3DTransformation _field);

class TBrownDiffuser{
	TRandom rand_source; 
	P3DTransformation field;	
	T3DCounter counter; 
	TBrownVectors *seed; 
	
	TPixelList crosses; 
	TPixelList *forward_pixel_list;
	TPixelList *backward_pixel_list;
	TCollector collector; 
	float b_factor; 
	float f_factor; 
	C3DUBImage *mask;
	
public:
	TBrownDiffuser(int seed_size, P3DTransformation _field, 
		       float _b_factor, float _f_factor, C3DUBImage *mask);
	~TBrownDiffuser();
	
	void add_pixels(int cnt);
	void move_pixels(float timestep);
	void remove_dead_pixels(); 
	
	TPixelList *get_backward_pixel_list()const; 
	TPixelList *get_forward_pixel_list()const; 
		
	void get_speed_list(TSpeedPixelQueue *collector, float speed_limit);	
	void get_critical_locations(TCriticalLocations *crosses, float max_dist_square);
	int get_collected()const;
private:	
	void move_pixel(TPixel *pixel, float timestep);
};


// inline implementations

inline int TBrownDiffuser::get_collected()const
{
	return collector.size();
}

inline TPixelList *TBrownDiffuser::get_backward_pixel_list()const
{
	return backward_pixel_list; 
}

inline TPixelList *TBrownDiffuser::get_forward_pixel_list()const
{
	return forward_pixel_list; 
}

#endif
