/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WIREFRAME_HH
#define __WIREFRAME_HH

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <viewit/drawable.hh>
#include <GL/glu.h>

#define WIREFRAME_CLASSNAME "TWireFrame"

class TPolyCone {
private:	
	unsigned int	__npoints;
	C3DDVector	*__point_array;
	double		*__radius_array;

public: 
	TPolyCone(unsigned int size);
	
	C3DDVector*	get_point_vec() const;
	double*		get_radius_vec() const;
	
	unsigned int size() const;
	
	void do_gl_draw();
};

class TWireFrameData {
private:
	
	std::list<TPolyCone *>   __paths;
	std::list<C3DFVector>	 __nodes;

	enum {ONLY_LINES=1, ONLY_POINTS=2, LINES_AND_POINTS=3 } __draw_components;
	
	GLUquadric *		__sphere_quad;

public:		
	TWireFrameData();
	~TWireFrameData();
	
	void add_polycone(TPolyCone *polycone);
	void add_node(const C3DFVector& point);

	void toggle_view();

	bool do_export(FILE *f) const;
	
	void do_gl_draw(const TCamera& c) const;
	
	bool do_handle_key_event(int key);
	bool do_handle_command_event(PEventInfo info);
};


class TWireFrame: public TDrawable {
private:
	class TWireFrameData *__data; 

protected:	
	class TWireFrameData *get_data() const;
	
	virtual void do_gl_draw(const TCamera& c) const; 
	virtual bool do_handle_key_event(int key);
	virtual bool do_handle_command_event(PEventInfo info);
	virtual bool do_export(FILE *f) const;

public:
	TWireFrame(const string& name);
	virtual ~TWireFrame();
	
	void add_node(const C3DFVector& point);
	void add_polycone(TPolyCone *polycone);
	
	virtual const char* get_classname() const;
	virtual void get_classname_list(list<string> *classlist) const;
};
#endif
