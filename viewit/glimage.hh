/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __glimage_h
#define __glimage_h



#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <viewit/slice.hh>
#include <viewit/texture.hh>
#include <viewit/planeexpose.hh>


#define GLSLICE_CLASSNAME "TGLSlice"
#define XYGLSLICE_CLASSNAME "TXYGLSlice"
#define XZGLSLICE_CLASSNAME "TXZGLSlice"
#define YZGLSLICE_CLASSNAME "TYZGLSlice"

extern const string type_TGLSlice;


///   The basic class to draw slices of 3D images in the 3D context.
class TGLSlice: public TSlice {
protected:	
	const C3DUBImage texture;
	P3DTransformation deformation;
	C3DBounds start,end;
	GL2DTexture *slice_texture;
	BasePlaneExpose *plane_exposed; 
	int max_slice;
	struct {
		int slice; 
		bool exposed; 
	} slice_state; 
	float transculence_thresh; 
public:	
	TGLSlice(const string& name, const C3DUBImage _texture, P3DTransformation _Deformation,const TColor& _color);
	~TGLSlice();
	
	void toggle_exposed();
	virtual void inc_slice();
	virtual void dec_slice();
	virtual void set_slice(float new_slice);
	virtual float get_slice()const;
	virtual void get_slice_range(float &from, float &to) const;
	void toggle_fine_display();
	const string& get_type() const;
	virtual const char* get_classname()const;
	virtual void get_classname_list(list<string> *classlist) const;

	virtual void set_translucence(float transculency);
	virtual float get_translucence() const;
	
	//	void povray_output_object(FILE *f)const{};
	//	void povray_output_attributes(FILE *f)const{};

protected: 
	virtual bool do_handle_key_event(int key); 
	virtual bool do_handle_command_event(PEventInfo info);	
private:
	virtual void do_gl_draw(const TCamera& c)const;
	virtual void draw_flat()const = 0;
	
	virtual void create_textures()= 0;
	virtual void gl_attach();
	virtual void gl_detach();	
};


///   The basic class to the axial slices of 3D images in the 3D context.
class TXYGLSlice: public TGLSlice {
public:
	TXYGLSlice(const string& name,const C3DUBImage _texture, P3DTransformation _Deformation);
private:
	
	virtual void draw_flat()const;
	virtual void create_textures();
	virtual void povray_output_object(FILE *f)const;
};

///   The basic class to the coronal slices of 3D images in the 3D context.
class TXZGLSlice: public TGLSlice {
public:
	TXZGLSlice(const string& name,const C3DUBImage _texture, P3DTransformation _Deformation);
private:
	
	virtual void draw_flat()const;
	virtual void create_textures();
	virtual void povray_output_object(FILE *f)const;
};

///   The basic class to the saggital slices of 3D images in the 3D context.
class TYZGLSlice: public TGLSlice {
public:
	TYZGLSlice(const string& name,const C3DUBImage _texture, P3DTransformation _Deformation);
private:
	
	virtual void draw_flat()const;
	virtual void create_textures();
	virtual void povray_output_object(FILE *f)const;	
};


#endif
