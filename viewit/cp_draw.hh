/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __cp_draw_h
#define __cp_draw_h

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <GL/glu.h>

#include <mia.hh>
#include <mia/3d/critical_point.hh>


#include <viewit/drawable.hh>
#include <viewit/texture.hh>

#include <map>

using namespace mia; 

/// a class to draw one critical point 
class TCPAppearance {
	C3DFVector loc[21];
	C2DFVector tex[21];
	TColor    col[21];
	static int indices[72];
	C3DFMatrix phase; 
	TColor *e1_color; 
	TColor *e2_color; 
	TColor *e3_color; 
		
	C3DFVector r1,r2,r3;
public:	
#if 0	
	TCPAppearance(const C3DFVector& l,const C3DFMatrix& _phase); 
#endif	
	TCPAppearance(const C3DCriticalPointEigen& in);
	void draw()const;
	void get_vertices(TVertexList *list)const;
	const C3DFVector& get_mid_point()const {
		return loc[0];
	}
	void povray_output_object(FILE *f)const;
private:
};

#define CPDRAWLIST_CLASSNAME "TDrawable"

/// a class to contain all critical points and draw them see also \link #TCPAppearance TCPAppearance \endlink
class  TCPDrawList : public TDrawable {
	struct cp_helper_t {
		const TCPAppearance *point;
		float z; 
		bool operator < (const cp_helper_t& a) const{
			return z < a.z; 
		}
	};

	const T2DAlphaSphere& alpha;
	
	typedef std::list<TCPAppearance> CCPList; 
	typedef std::priority_queue<cp_helper_t> TCPOrder;
	
	CCPList list; 
public:
	TCPDrawList(const string& name,const T2DAlphaSphere& _alpha);
	void add(const TCPAppearance& cp);

	virtual const char *get_classname() const;
	virtual void get_classname_list(std::list<string> *classlist) const;

	void clear();
	void get_vertex_list(TVertexList *list)const;
	virtual void povray_output_object(FILE *f)const;
	virtual void gl_attach();
private:
	void do_gl_draw(const TCamera& c)const; 
};

inline void TCPDrawList::add(const TCPAppearance& cp)
{
	list.push_back(cp);
}
inline void TCPDrawList::clear()
{
	list.clear();
}
     
#endif
