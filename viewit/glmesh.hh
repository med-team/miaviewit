/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __glmesh_hh
#define __glmesh_hh

#include <mia.hh>
using namespace mia; 


#include <viewit/drawable.hh>
#include <string>
using namespace std; 

class GL3DGrayTexture ; 

#define GLMESH_CLASSNAME "TGLMesh"

enum EScaleType  { 
	md_standart,  /**<! standart full range mapping */
	md_divide,    /**<! color only reffering to growth/ no change/shrink */
	md_log,       /**<! use a logarithmic full range scale */
	md_scale_2mm, /**<! maximum color for deformation above 2mm */
	md_scale_4mm, /**<! maximum color for deformation above 4mm */
	md_scale_8mm, /**<! maximum color for deformation above 8mm */
	md_scale_16mm,/**<! maximum color for deformation above 16mm */
	md_scale_32mm,/**<! maximum color for deformation above 32mm */
	md_last       /**<! value to indicade end of range */
};


/** This class propagates a trianglemesh to a drawable with all the bells and whistles
  */

class TGLMesh: public TDrawable {
public:
	/** constructor of the class 
	    \param mesh the mesh we want to show 
	 */
	
	TGLMesh(const string& name, PTriangleMesh mesh);
	
	/// the destructor 
	~TGLMesh();
	
	/** set a texture for the mesh 
	    \param texture the texture to be set, or NULL to unset the texture
	*/
	void set_texture(GL3DGrayTexture  *texture);
	
	/** get the underlying mesh
	    \returns mesh ptr
	*/
	CTriangleMesh *get_mesh();
	
	
	/** draw the decoration of this mesh
	    \param viewport where to draw
	    \param writer the writer used to draw with
	    \remark make this a const member function
	 */
	void draw_decoration(const C2DBounds& viewport,const TGLText& writer);

	virtual void get_classname_list(list<string> *classlist)const;
	virtual const char* get_classname()const;
	
	/** attach the mesh to the gl context */
	virtual void gl_attach();

	void apply_color_change();
	
private:
	/** handles the following key events
	    a - change color scale
	    d - toggle decoration display 
	    f - tollgle front-/back-faceculling 
	    g - toggle glossy surface
	    k - toggle culling
	    l - toggle light model (one-side/two-side)
	    \todo d should go into TDrawable
	 */
	bool do_handle_key_event(int key);
	bool do_handle_command_event(PEventInfo info);
	void do_gl_draw(const TCamera& c) const;

	
	struct TGLMeshData *data; 
};

#endif
