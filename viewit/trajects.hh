/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __trajects_hh
#define __trajects_hh

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <mia.hh>
using namespace mia; 

#include <memory>

#include <viewit/drawable.hh>
#include <viewit/pixel.hh>
#include <viewit/texture.hh>


/// a datatype to trace describe trajectories in the vector field
struct C3DTracepoint {
	C3DFVector location; /*!< the locaton of the trajectory point */
	C3DFVector tangent;  /*!< the diretction of the field at this point */
	float weight;         /*!< the weighted norm of the field at this point */
};

typedef std::list<C3DTracepoint> C3DTrajectory; 

struct stream_element_t {
	TColor col;
	C4DFVector tex;
	C3DFVector loc;
};
#define NUM_ALPHA_STAGES 16

/// a class to draw Trajectories as lines (no longer used)
class TTrajectories:public TDrawable {
	P3DTransformation field;
	std::shared_ptr<GL2DHTexture> texture;	
	static float alpha_shape[NUM_ALPHA_STAGES];
	typedef std::list<stream_element_t> TStreamline; 
	typedef std::list<TStreamline> TFlowfield;
	typedef std::list<C3DTrajectory> CTrajectoryList;
	
	TFlowfield flow; 
public:
	TTrajectories(const string& name, P3DTransformation _field, std::shared_ptr<GL2DHTexture> _texture);
	TTrajectories(const string& name, const TVertexList& start_list, 
		      P3DTransformation _field, std::shared_ptr<GL2DHTexture> _texture);	
	void add(const TVertexList& start_list); 
	void add(const TSpeedPixelQueue& start_list,size_t max_size, float min_distance); 
	void clear();
	const string& get_type() const;
private:
	void generate_trace_list(TVertexList *start);
	float get_trajectory_from(const C3DFVector& Start,
				  float step, size_t sizelimit,
				  float normlimit, 
				  C3DTrajectory& trace);
	
	virtual void povray_output_object(FILE *f)const;
	virtual void povray_output_description(FILE *f)const;
	void do_gl_draw(const TCamera& c)const;
};

/// a class to find trajectories in vectorfields
class TFieldTracer {
	struct TFieldTracerData *data; 
public:
	TFieldTracer(P3DTransformation field_, float min_norm_, float min_dist_);
	~TFieldTracer();
	shared_ptr<TVertexList> get_start_vertexes();
	shared_ptr<TVertexList> get_start_vertexes(const TVertexList& suggest);
};


#endif
