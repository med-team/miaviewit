/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <viewit/snapshot.hh>
#include <mia/2d/rgbimageio.hh>
#include <GL/gl.h>
#include <mia.hh>
#include <queue>
#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>

using namespace std; 
using namespace mia; 




class CSnapshotQueue  {
public: 
	CSnapshotQueue(); 
	
	void push(CRGB2DImage *foto);
	std::shared_ptr<CRGB2DImage> top_and_pop(); 
	bool empty(); 
private: 
	queue<std::shared_ptr<CRGB2DImage> > m_queue; 
	std::mutex m_mutex; 
	std::condition_variable m_cond;
};

CSnapshotQueue::CSnapshotQueue()
{
}

void CSnapshotQueue::push(CRGB2DImage *foto)
{
	std::unique_lock<std::mutex> lock(m_mutex); 
	const bool was_empty=m_queue.empty();
	m_queue.push(std::shared_ptr<CRGB2DImage>(foto)); 
	lock.unlock(); 
	if (was_empty) {
		cvdebug() << "notify saver thread\n"; 
		m_cond.notify_one();
	}
}

std::shared_ptr<CRGB2DImage> CSnapshotQueue::top_and_pop()
{
	std::unique_lock<std::mutex> lock(m_mutex); 
	assert(!m_queue.empty()); 
	auto retval = m_queue.front(); 
	m_queue.pop(); 
	return retval; 

}

bool CSnapshotQueue::empty()
{
	// if the queue is empty, then we wait for some time 
	// we do not completely block the thread in order to enable it 
	// to cleanly exit 
	std::unique_lock<std::mutex> lock(m_mutex);
	std::chrono::milliseconds dt(10);
	if (m_queue.empty()) 
		m_cond.wait_for(lock, dt);
	return m_queue.empty(); 
}

class CSnapshotServer  {
public: 	
	CSnapshotServer(CSnapshotQueue& queue);
	CSnapshotServer(const CSnapshotServer& orig); 

	void stop(); 
	void operator() (); 
private: 
	CSnapshotQueue& m_queue; 
	size_t m_id; 

	// this should be atomic
	std::atomic<bool> m_run; 
};

CSnapshotServer::CSnapshotServer(CSnapshotQueue& queue):
	m_queue(queue), 
	m_id(0)
{
        m_run = true; 
}

CSnapshotServer::CSnapshotServer(const CSnapshotServer& orig):
	m_queue(orig.m_queue), 
	m_id(orig.m_id)
{
	m_run = true; 
}

void CSnapshotServer::stop()
{
	cvdebug() << "stop snapshot server\n"; 
	m_run = false; 
}

void CSnapshotServer::operator() ()
{

	CThreadMsgStream thread_stream;
	cvdebug() << "start snapshot server\n"; 
	while (m_run) {
		if (m_queue.empty()) 
			continue; 
		auto image = m_queue.top_and_pop(); 
		assert(image); 
		stringstream fname ; 

		fname << "viewit-screenshot-" << m_id++ << ".png"; 
		cvdebug() << "save snapshot to " << fname.str() << "\n"; 
		save_image(fname.str(), *image);
	}
	cvdebug() << "Exit snapshot server\n"; 
}

struct CSnapshotClientImpl {
	CSnapshotClientImpl();
	~CSnapshotClientImpl();
	void foto(const C2DBounds& camera);

	CSnapshotQueue m_queue;
	CSnapshotServer m_server;
	std::thread m_server_thread; 
	CThreadMsgStream thread_stream;
}; 


CSnapshotClientImpl::CSnapshotClientImpl():
	m_queue(), 
	m_server(m_queue),
	m_server_thread(m_server)
{
}

CSnapshotClientImpl::~CSnapshotClientImpl()
{
	m_server.stop(); 
	m_server_thread.join(); 
}
	
void CSnapshotClientImpl::foto(const C2DBounds& viewport)
{ 
	cvdebug() << "Take picture of size " << viewport << "\n"; 
	
	vector<unsigned char> buffer(viewport.x * viewport.y * 3); 
        glFlush();
	
        glReadBuffer(GL_BACK);
        glPixelStorei(GL_PACK_ALIGNMENT,1);
        glReadPixels(0,0,viewport.x,viewport.y,GL_RGB,GL_UNSIGNED_BYTE, &buffer[0]);
	// flip along y
	
	auto image = new CRGB2DImage(viewport);
	auto b = buffer.begin(); 
	
	const int stride = viewport.x * 3; 
	auto p = image->pixel() + (viewport.y -1) * stride; 

	for (auto y = 0u; y < viewport.y; ++y, b += stride, p -= stride)
		copy(b, b+stride, p); 

        m_queue.push(image);
}

CSnapshotClient::CSnapshotClient()
{
	pimpl = new CSnapshotClientImpl(); 
}

CSnapshotClient::~CSnapshotClient()
{
	delete pimpl; 
}


void CSnapshotClient::foto(const C2DBounds& viewport)
{
	pimpl->foto(viewport); 
}
