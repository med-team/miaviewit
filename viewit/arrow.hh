/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __arrow_hh
#define __arrow_hh

#include <mia.hh>

#include <viewit/drawable.hh>
#include <stdio.h>

#include <viewit/structures.hh>

#define ARROWLIST_CLASSNAME "TArrowList"
#define ARROWLISTGROW_CLASSNAME "TArrowListGrow"



/// a class to draw some arrows : set size is predefined
class TArrowList: public TDrawable {
	class TArrowListData *data; 
public:	
	TArrowList(const string& name,const TColor& shaft_color, const TColor& hat_color,
		   const TNormalVertexList& vl, const C3DTransformation&  deform); 
	
	TArrowList(const string& name,const TColor& _start_color, const TColor& _end_color,
		   const TVertexList& vl, const C3DTransformation& deform);
	
	
	TArrowList(const string& name,const TColor& shaft_color, const TColor& hat_color); 
	
	TArrowList(const string& name,const TColor& shaft_color, const TColor& hat_color, FILE *f,
		   P3DTransformation deform); 
	
	template <class InputIterator>	
	TArrowList(const string& name,const TColor& shaft_color, const TColor& hat_color,
		   InputIterator begin, InputIterator end,  const C3DTransformation& deform_);
	
	
	TArrowList(const string& name,const TColor& shaft_color, const TColor& hat_color, FILE *f);
	
	TArrowList(const string& name);

	virtual const char* get_classname()const;
	virtual void get_classname_list(list<string> *classlist)const;
	
	~TArrowList();
	int size()const; 
	
	void double_arrow_number();
	void half_arrow_number();
	void increase_arrow_size();
	void decrease_arrow_size();
	
	virtual void gl_attach();
	virtual void gl_detach();
	
protected:	
	class TArrowListData *get_data()const;
private:	
	virtual void do_gl_draw(const TCamera& c) const; 
	virtual bool do_handle_key_event(int key);
	virtual bool do_handle_command_event(PEventInfo info);
	virtual bool do_export(FILE *f)const;
};



/// a class to draw some arrows : arrows can be added later
class TArrowListGrow: public TArrowList {
	P3DTransformation deform; 
public:
	TArrowListGrow(const string& name,const TColor& _start_color, const TColor& _end_color,
		       P3DTransformation deform_);
	TArrowListGrow(const string& name,const TColor& shaft_color, const TColor& hat_color, 
		       FILE *f, P3DTransformation deform_);
	
	
	TArrowListGrow(const string& name, P3DTransformation deform);
	void add_vector(const C3DFVector& v);
	void set_vectorfield(P3DTransformation deform_);	
	void clear();
	
	virtual const char* get_classname()const;
	virtual void get_classname_list(list<string> *classlist)const;
};



class TArrow {
	C3DFVector start, dir; 
	float matrix[16];
public: 
	TArrow(const C3DFVector& start, const C3DFVector& end);
	void init_position()const;
	void gl_attach();
	void flip();
	bool do_export(FILE *f)const;
	friend C3DFVector operator - (const TArrow& a, const TArrow& b);
};


class  TArrowListData {
	static float bottom_normal[3];
	static C3DFVector side_quad_strip_normal[9];
	static C3DFVector side_quad_strip_vertex[18];
	static C3DFVector top_normal[8];
	static unsigned char bottom[24];
	static unsigned char side_quad_strip[18];
	static unsigned char hat_bottom_quad_strip[18];
	static int top[24];
	static float vertices[ 3 * 26 ];
	float magnify; 
	int n_draw_arrow_step; 
	list<TArrow> arrow_list;
	
	GLuint arrow_disp_list; 
	
public:	
	TArrowListData():magnify(1.0f),n_draw_arrow_step(1){};
	
	void draw_hat()const;
	void draw_shaft()const;
	
	void draw_bottom()const; 
	
	void add_arrow(const TArrow& arrow);
	
	void clear();
	
	void gl_attach();
	void gl_detach();
	
	bool do_export(FILE *f)const;
	
	void do_gl_draw(const TCamera& c) const;
	bool do_handle_key_event(int key);
	bool do_handle_command_event(PEventInfo info);
	
	GLuint get_arrow_disp_list()const {
		return arrow_disp_list;
	}
	int size()const {
		return arrow_list.size();
	}
};



extern TColor bottom_color;


template < class InputIterator>	
TArrowList::TArrowList(const string& name,const TColor& shaft_color, const TColor& hat_color,
		       InputIterator begin, InputIterator end, const C3DTransformation& deform):
	TDrawable(name),
	data(new TArrowListData())
{
	
	set_color1(shaft_color);
	set_color2(hat_color);
	set_color3(bottom_color);
	
		
	for (; begin != end; ++begin) {
		C3DFVector v = deform.apply( *begin);
		if (v.norm2() > 0.5) {
			data->add_arrow( TArrow(*begin + v, *begin )); 
		}
	}
	light_on();
}



#endif
