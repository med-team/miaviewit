/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __glerror_hh
#define __glerror_hh

#include <GL/gl.h>
#include <string>
/**
   A exception class to give some more information about an OPenGL error.
   Namely it is able to convert an OpenGL error number to the respective error string.
 */

/// a class to throw exceptions based on OpenGL error numbers

class gl_error: public std::exception {
	/// contains the error string
	std::string _what; 
public:
	/** A standart constructor 
	    \remarks Why is this one needed ?
	*/
	gl_error();
	
	/** The destructor that is not allowed to throw. */
	~gl_error() throw(){};
	
	/** construct the exception 
	    \param err an OpenGL error number
	*/
	gl_error(GLenum err);
	
	/** construct the exception 
	    \param msg a message string
	 */
	gl_error(const char *msg);
	
	/** construct the exception 
	    \param msg a message string
	    \param err an OpenGL error number
	*/
	gl_error(const char *msg, GLenum err);
	
	/** \returns the error string as C-string */
	virtual const char* what () const throw() { return _what.c_str (); }
};

#endif
