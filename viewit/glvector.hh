/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __glvector_h
#define __glvector_h

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <GL/gl.h>
#include <stdio.h>

#include <mia.hh>
using namespace mia; 

#include <viewit/drawable.hh>
#include <viewit/texture.hh>

class TGLVector {
	C3DFVector start; 
	C3DFVector end;
	C4DFVector tangent; 
public:
	TGLVector(const C3DFVector& start, const C3DFVector& end);
	void glVertexStart()const;
	void glVertexEnd()const;
	void povray_output(FILE *f,const TColor& foot_color,const TColor& hat__color) const;
	void glTex()const {
		glTexCoord4fv(&tangent.x);
	}
};


class TGLVectorList:public TDrawable  {
	GL2DHTexture *texture;
public:
	typedef std::list<TGLVector> TGLVectors;
	TGLVectors list; 
	
	TGLVectorList(const string& name,const TColor& _start_color, const TColor& _end_color,
		      const TVertexList& vl, const C3DTransformation& deform, GL2DHTexture *_texture);

private:
	virtual void do_gl_draw(const TCamera& c) const; 
	virtual void povray_output_object(FILE *f)const;
};

// inline implementation
inline TGLVector::TGLVector(const C3DFVector& _start, const C3DFVector& _end):
	start(_start),end(_end)
{
	tangent.x = end.x - start.x;
	tangent.y = end.y - start.y;
	tangent.z = end.z - start.z;
	tangent.a = 0.0f; 
	float norm = tangent.norm();
	if (norm > 0)
		tangent /= norm;
	tangent.a = 1.0f; 
}


inline void TGLVector::glVertexStart()const
{
	glVertex3fv(&start.x);
}

inline void TGLVector::glVertexEnd()const
{
	glVertex3fv(&end.x);
}

#endif
