/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include <viewit/glerror.hh>
#include <GL/glu.h>
#include <sstream>

gl_error::gl_error(GLenum errn)
{
	_what = std::string((char*)gluErrorString(errn));
}

gl_error::gl_error()
{
	_what = std::string((char*)gluErrorString(glGetError()));

}
gl_error::gl_error(const char *info)
{
	std::stringstream msg; 
	msg << info << ":" << gluErrorString(glGetError()); 
	_what = msg.str();
}

gl_error::gl_error(const char *info,GLenum errn)
{
	std::stringstream msg; 
	msg << info << ":" << gluErrorString(errn); 
	_what = msg.str();
}
