/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __camera_hh
#define __camera_hh

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <mia.hh>
using namespace mia; 

#include <GL/gl.h>

#include <viewit/event.hh>


#define EV_CAMERA_TIME_MEASURE     0x1001
#define EV_CAMERA_SET_PREDEF_VIEW  0x1002
#define EV_CAMERA_GET_PREDEF_VIEW  0x1003

class TCamera: public TEventHandler {

public:
	class TLocation: public CEventInfo {
		friend class TCamera;
		friend class TCameraData;
		
		float distance;
		float zoom;
		C4DFVector rot;
		C3DFVector mid_shift;
	public:
		TLocation();
		TLocation(float _distance, const C4DFVector& _rot,float _zoom,const C3DFVector& _mid_shift);
		virtual void read(std::istream& is);
		virtual void write(std::ostream& os);
	};

	TCamera();
	TCamera(float _distance, const C4DFVector& _rot,float _zoom,const C3DFVector& _mid_shift);
	virtual ~TCamera();
	
	void gl_set_fustrum(const C2DBounds& new_viewport);
	void gl_use_camera(); 

	void heading(float angle);
	
	C3DFVector transform(const C3DFVector& v)const; 
#if 0 	
	C3DFVector inv_transform(const C3DFVector& v)const;
#endif	
	float get_z_dist(const C3DFVector& v)const;
	const C2DBounds& get_viewport() const;
	const C3DFVector& get_location() const;
	const C3DFVector& get_mid_shift() const;
	bool low_detail()const; 
	const C3DFVector get_farplane_spacecoord(const C2DFVector& screen_coord)const;
	const C3DFVector get_nearplane_spacecoord(const C2DFVector& screen_coord)const;	
	const C3DFVector get_spacecoord(const C2DFVector& screen_coord, float zvalue)const; 
	const C3DFVector get_spacecoord(int x, int y)const;
protected:	
	
	//	bool do_handle_key_event(int key); 
	bool do_handle_mouse_event(int button,int x, int y); 
	virtual bool do_handle_command_event(PEventInfo info);
private:
	class TCameraData *data; 
};

#endif
