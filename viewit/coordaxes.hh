/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __COORDAXES_HH
#define __COORDAXES_HH

#include <viewit/drawable.hh>

#define COORDAXES_CLASSNAME "TCoordAxes"

/// a class to handle OpenGL coordinate system (axes)

class TCoordAxes : public TDrawable {

protected:
	C3DFVector __origin;
	bool do_draw_decoration;

public:	
	TCoordAxes(const string& name);
	TCoordAxes(const string& name, const C3DFVector& origin);	
	~TCoordAxes();
	
	virtual const char* get_classname()const;
	virtual void get_classname_list(list<string> *classlist) const;
	virtual void draw_decoration(const C2DBounds& viewport,const TGLText& writer);
	
protected:
	/** 
	    The Key-event handler for a TCoordAxes object. It obeys the following keys:
	    \begin{itemize}
	    \item 'd' - toggle the drawing of the legend
	    \end{itemize}
	*/
	virtual bool do_handle_key_event(int key); 
	virtual bool do_handle_command_event(PEventInfo info);
	virtual void do_gl_draw(const TCamera& c) const;
};

#endif
