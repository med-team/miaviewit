/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __light_hh
#define __light_hh

#include <viewit/drawable.hh>


/// a light in the scene - maybe this should be derived from TEventHandler
class TLight: public TDrawable {
public:
	TLight(const C3DFVector& position_, const TColor& color_, int no_);
	~TLight();
	void on()const;
	void off()const;
	void gl_use() const;
protected:
	void do_gl_draw(const TCamera& c) const;
	bool do_handle_key_event(int key);
	bool do_handle_mouse_event(int button,int x, int y);
private: 
	class TData *data; 
};


#endif
