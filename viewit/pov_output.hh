/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __pov_output_h
#define __pov_output_h


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>

#include <viewit/drawable.hh>

class TPovOutput {
	FILE *file;
public:
	TPovOutput(int num);
	~TPovOutput();
	void init(const C3DFVector& trans, const C3DFVector& rot, const C3DFVector& shift,float width,float height);
	void write(const TDrawMap& data);
	void finish();
};


#endif
