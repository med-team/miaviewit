/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include <viewit/slice.hh>


bool TSlice::do_handle_key_event(int key)
{
	switch (key) {
	case 'g':dec_slice();break;
	case 'G':inc_slice();break;
	default: 
		return TDrawable::do_handle_key_event(key); 
	}
	return true; 
}

bool TSlice::do_handle_command_event(PEventInfo info)
{
	switch (info->get_command()) {
		
	case EV_SLICE_GET_STATE: {
		TSliceEventInfo& ev = dynamic_cast<TSliceEventInfo&>(*info);
		ev.set_slice_nr(get_slice());
		float from,to;
		get_slice_range(from,to);
		ev.set_slice_range(from,to);
		return true;
	}
	case EV_SLICE_UP:  
		inc_slice();
		return true; 
		
	case EV_SLICE_DOWN:
		dec_slice(); 
		return true; 
		
	case EV_SLICE_SET_SLICE: {
		TSliceEventInfo& ev = dynamic_cast<TSliceEventInfo&>(*info);
		set_slice(ev.get_slice_nr());
		return true; 
	}
	} // end switch
	
	return TDrawable::do_handle_command_event(info);	
}
const char* TSlice::get_classname() const 
{
	return SLICE_CLASSNAME;
}

void TSlice::get_classname_list(list<string> *classlist) const
{
	if (classlist) {
		classlist->push_front(SLICE_CLASSNAME);
		TDrawable::get_classname_list(classlist);
	}
}
