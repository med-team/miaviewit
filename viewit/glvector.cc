/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include <viewit/glvector.hh>

TGLVectorList::TGLVectorList(const string& name,const TColor& _start_color, const TColor& _end_color,
			     const TVertexList& vl, const C3DTransformation& deform, GL2DHTexture *_texture):
	TDrawable(name),
	texture(_texture)
	
{
	set_color1(_start_color);
	set_color2(_end_color);
	for (TVertexList::const_iterator i = vl.begin(); i != vl.end(); ++i) {
		C3DFVector v = deform.apply(*i);
		if (v.norm2() > 0.25) {
			list.push_back(TGLVector(*i, *i - v));
		}
	}
	transparent_on();
}

void TGLVector::povray_output(FILE *f,const TColor& foot_color,const TColor& hat_color)const
{
	C3DFVector middle = start + (end-start) * 0.8f;
	fprintf(f,"cone { <%f, %f, %f> , 0.7, ",middle.x,middle.y,middle.z);
	fprintf(f,"<%f, %f, %f>, 0.0 ",end.x,end.y,end.z);
	fprintf(f,"texture { pigment {color rgb <%f, %f %f> } } }\n", hat_color.x,hat_color.y,hat_color.z);
		 
	fprintf(f,"cylinder { <%f, %f, %f>, ",start.x,start.y,start.z);
	fprintf(f," <%f, %f, %f> , 0.2 ",end.x,end.y,end.z);
	fprintf(f,"texture { pigment {color rgb <%f, %f %f> } } }\n", 
		 foot_color.x,foot_color.y,foot_color.z);
	
}

void TGLVectorList::do_gl_draw(const TCamera& c) const
{
	if (!is_visible())
		return; 
	

	glEnable(GL_LINE_SMOOTH);
	if (texture) {
		texture->gl_texture_matrix_on();
		glEnable(GL_TEXTURE_2D);
		texture->bind();
	}
	
	glBegin(GL_LINES); 
	
	for (TGLVectors::const_iterator i = list.begin(); 
	     i != list.end(); i++) {
		glColor4fv(&get_color1().x);
		(*i).glTex();
		(*i).glVertexStart();
		glColor4fv(&get_color2().x);
		(*i).glVertexEnd();
	}
	glEnd();

	if (texture) {
		texture->gl_texture_matrix_off();
		glDisable(GL_TEXTURE_2D);
	}
	glDisable(GL_LINE_SMOOTH);
}

void TGLVectorList::povray_output_object(FILE *f)const
{
	for (TGLVectors::const_iterator i = list.begin(); 
	     i != list.end(); i++) {
		(*i).povray_output(f,get_color1(),get_color2());
	}
}
