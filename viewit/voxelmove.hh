/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __voxelmove_hh
#define __voxelmove_hh

#include <memory>

#include <viewit/drawable.hh>
#include <viewit/voxelmover.hh>
#include <mia.hh>


using namespace mia; 

typedef T3DVectorSource<C3DFVector> C3DFVectorSource;
typedef TBrownVoxelMover<C3DTransformation, C3DFVector> TDeformation; 

#define VOXELCLOUD_CLASSNAME "TVoxelCloud"

class TVoxelCloud : public TDrawable {
	typedef std::list<C3DFVector> TVoxelList; 
	
	const TDeformation& _M_mover;
	float _M_time_step;
	float _M_voxel_lifetime;
	TVoxelList _M_forward_voxels; 
	TVoxelList _M_backward_voxels;
	float _M_age; 
	C3DFVectorSource _M_source; 
	int _M_size; 
public:	
	TVoxelCloud(const string& name,int __size, 
		    float __time_step,
		    float __voxel_lifetime, 
		    const TDeformation& __deformation,
		    const C3DFVectorSource& __source);


	shared_ptr<TVertexList> get_vertex_list(int stride)const; 
	virtual bool animate();
	
	void toggle_moving();
	void init_voxels();
	
	void set_lifetime(float lifetime);
	float get_lifetime() const;
	virtual const char* get_classname()const;
	virtual void get_classname_list(list<string> *classlist)const;
	
private:
	virtual void do_gl_draw(const TCamera& c) const;
	virtual bool do_handle_key_event(int key);
	virtual bool do_handle_command_event(PEventInfo info);	
};

#endif
