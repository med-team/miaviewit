/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __VISTALOADER_HH
#define __VISTALOADER_HH

#include <string>

#include <vistaio.h>
#include <mia.hh>
using namespace mia; 

class TDrawable;

TDrawable* load_tensorfield(const std::string& name, const std::string& filename);

TDrawable* load_wireframe(const std::string& name, const std::string& filename);



bool load_texture_images(const std::string& filename, P3DTransformation defo,
			 TDrawable **xy_slice, const std::string& xy_slice_name,
			 TDrawable **xz_slice, const std::string& xz_slice_name, 
			 TDrawable **yz_slice, const std::string& yz_slice_name);



#endif
