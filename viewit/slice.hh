/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __slice_hh
#define __slice_hh

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <viewit/drawable.hh>

#define SLICE_CLASSNAME "TSlice"

class TSlice: public TDrawable {
public:
	TSlice(const string& name):TDrawable(name){}
	virtual void inc_slice()=0;
	virtual void dec_slice()=0;
	virtual void set_slice(float new_slice)=0;
	virtual float get_slice()const=0;
	virtual void get_slice_range(float &from, float &to)const=0;
	virtual const char* get_classname()const;
	virtual void get_classname_list(list<string> *classlist)const;
	
protected: 
	virtual bool do_handle_key_event(int key); 
	virtual bool do_handle_command_event(PEventInfo info);	
};

#endif
