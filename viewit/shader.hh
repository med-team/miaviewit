/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __shader_hh
#define __shader_hh

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <mia.hh>
using namespace mia; 


#include <viewit/4DVector.hh>
#include <GL/gl.h>
#include <GL/gle.h>

#include <glibmm/objectbase.h>


#define TEXTURE_ARRAY_SIZE_HEIGHT 4
#define TEXTURE_ARRAY_SIZE_WIDTH  4

/** the basic CShape, it does nothing */
class CShading: public Glib::ObjectBase {
public:
	
	/** pseudo-abstract funtion to prepare the shading */
	virtual void pre_draw();

	/** pseudo-abstract funtion to prepare the shading */
	virtual void post_draw();

	/** pseudo-abstract funtion to generate necessary GL-datastructs */
	virtual void gl_attach();
	
	/** pseudo-abstract funtion to release necessary GL-datastructs */
	virtual void gl_detach();
};

/** the CColorGradientSphereShading, initially thought 
    to dye the sphere vertices corresponding to the
    distance of the vertex to the sphere center
    \remark didn't find a useful way to do this, used a simple texture instead (not useful too)
 */
class CColorGradientSphereShading : public CShading {
private:
	GLuint			m_texture_name;
	T4DVector<GLubyte>*	m_texture;
	
public:	
	CColorGradientSphereShading(const C3DFVector &v, const float &vmax);
	~CColorGradientSphereShading();

	virtual void post_draw();
	virtual void pre_draw();	

	virtual void gl_attach();
	virtual void gl_detach();
};


/** the CColorShading,
    dyes an object with a given color
 */
class CColorShading : public CShading {
private:
	C4DFVector	m_color;

public:	
	CColorShading(const C3DFVector &color);
	~CColorShading();

	virtual void post_draw();
	virtual void pre_draw();	
};

#endif
