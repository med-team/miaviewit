/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include <viewit/shader.hh>

#define CUBE_MAP_SIZE 1

void CShading::pre_draw()
{
}

void CShading::post_draw()
{
}

void CShading::gl_attach()
{
}
	
void CShading::gl_detach()
{
}
	

CColorGradientSphereShading::CColorGradientSphereShading(const C3DFVector &v, const float &vmax)
	: m_texture(new T4DVector<GLubyte>[TEXTURE_ARRAY_SIZE_HEIGHT * TEXTURE_ARRAY_SIZE_WIDTH])
{
	C3DFVector Color1(255,   0,   0);
	C3DFVector Color2( 50,  50,  50);
	C3DFVector Color3(  0,   0, 255);

	// north pole (z-axis)
	C3DFVector temp1;
	C3DFVector temp2;
	
	if (v.z < 1.0) {
		temp1  = Color1;
		temp1 *= 1.0 - v.z;
		temp2  = Color2;
		temp2 *= v.z;
	} else {
		temp1  = Color3;
		temp1 *= v.z / vmax;
		temp2  = Color2;
		temp2 *= 1.0 - (v.z / vmax);
	}
	temp1 += temp2;

	T4DVector<GLubyte> temp3(GLubyte(temp1.x), GLubyte(temp1.y), GLubyte(temp1.z), 255);
	
	m_texture[0] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[1] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[2] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[3] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	// south pole (z-axis)
	m_texture[12] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[13] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[14] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[15] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	
	// x-axis
	if (v.x < 1.0) {
		temp1  = Color1;
		temp1 *= 1.0 - v.x;
		temp2  = Color2;
		temp2 *= v.x;
	} else {
		temp1  = Color3;
		temp1 *= v.x / vmax;
		temp2  = Color2;
		temp2 *= 1.0 - (v.x / vmax);
	}
	temp1 += temp2;

	temp3 = T4DVector<GLubyte>(GLubyte(temp1.x), GLubyte(temp1.y), GLubyte(temp1.z), 255);
	
	m_texture[4] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[5] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[8] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[9] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);

	// y-axis
	if (v.y < 1.0) {
		temp1  = Color1;
		temp1 *= 1.0 - v.y;
		temp2  = Color2;
		temp2 *= v.y;
	} else {
		temp1  = Color3;
		temp1 *= v.y / vmax;
		temp2  = Color2;
		temp2 *= 1.0 - (v.y / vmax);
	}
	temp1 += temp2;

	temp3 = T4DVector<GLubyte>(GLubyte(temp1.x), GLubyte(temp1.y), GLubyte(temp1.z), 255);
	
	m_texture[6] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[7] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[10] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
	m_texture[11] = temp3; // T4DVector<GLubyte>(temp1.x, temp1.y, temp1.z, 255);
}



CColorGradientSphereShading::~CColorGradientSphereShading()
{
	delete [] m_texture;
}


void CColorGradientSphereShading::pre_draw() {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_texture_name);
}
	

void CColorGradientSphereShading::post_draw()
{
	glDisable(GL_TEXTURE_2D);
}


void CColorGradientSphereShading::gl_attach() 
{
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_FLAT);
	glGenTextures(1,&m_texture_name);
	glBindTexture(GL_TEXTURE_2D, m_texture_name);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);	

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TEXTURE_ARRAY_SIZE_WIDTH,
		     TEXTURE_ARRAY_SIZE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE,
		     &m_texture[0].x);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
}
	


void CColorGradientSphereShading::gl_detach()
{
	glDeleteTextures(1,&m_texture_name);
}
		

CColorShading::CColorShading(const C3DFVector& color)
	: m_color(color.x, color.y, color.z, 1.0)
{}


CColorShading::~CColorShading()
{}

void CColorShading::post_draw() 
{}
	
void CColorShading::pre_draw()
{
	glMaterialfv(GL_FRONT,GL_DIFFUSE,&m_color.x);	
	
}

