/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __shape_hh
#define __shape_hh

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glibmm/objectbase.h>
#include <mia.hh>
using namespace mia; 

/** the basic CShape, it does nothing */
class CShape: public Glib::ObjectBase {
public:
	
	/** pseudo-abstract funtion to prepare the shape */
	virtual void pre_draw();


	/** pseudo-abstract funtion to generate necessary GL-datastructs */
	virtual void gl_attach();
	
	/** pseudo-abstract funtion to release necessary GL-datastructs */
	virtual void gl_detach();
};

/** a sherical shape */
class CSphereShape: public CShape {
public:	
	/** set the sphere to a certain radiaus 
	    \param radius the radius of the sphere 
	 */
	CSphereShape(float radius);
	
	/// apply the scaling of the sphere
	virtual void pre_draw();
private:
	float m_radius; 
};

/** a basic CDeformedShape, it does nothing */
class CDeformedShape: public CShape {
public:	
	/** apply the rotation and scaling */
	virtual void pre_draw();
	
	/** prepare the rotation and scaling state */
	virtual void gl_attach();

	/** release the GL state */
	virtual void gl_detach();
private:
	virtual void make_gl_op()=0;
};

/** an elipsoid shape **/
class CEllipsoidShape: public CDeformedShape {
public:
	/** create the shape by giving the scaling factor and the rotation 
	    \param scale the scale in the three coordinate directions
	    \param rot the rotation out of axis alignment
	 */
	CEllipsoidShape(const  C3DFVector& scale, const C3DFVector& rot);
	
private:
	virtual void make_gl_op();
	
	C3DFVector m_scale;
	C3DFVector m_rot;
};

/** a free shape **/
class CFreeShape: public CDeformedShape {
public:
	/** create the shape by giving the transformation matrix
	    \param defo the transformation matrix
	*/
	CFreeShape(const C3DFMatrix& defo);
private:
	virtual void make_gl_op();
	float m_defo[16];
};


#endif
