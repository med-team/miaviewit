/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __volumerenderer_hh
#define __volumerenderer_hh

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <GL/gl.h>
#include <mia.hh>
#include <stdio.h>
#include <iostream>
#include <viewit/4DVector.hh>
#include <viewit/drawable.hh>
#include <viewit/camera.hh>



#define VOLUMERENDERER_CLASSNAME "TVolumeRenderer"

class TVolumeRenderer: public TDrawable {

protected:	
	
	virtual void do_gl_draw(const TCamera& c) const; 
	virtual void gl_attach();
	virtual void gl_detach();
	virtual bool do_handle_key_event(int key);
	virtual bool do_handle_command_event(PEventInfo info);

public:

	typedef std::vector<C4DFVector> TransferTable; 
	
	GLfloat		threshold;		//variable for opacity control
	GLfloat		slice_multiplier;	//setting the amont of slices
	
	TVolumeRenderer(const string& name, const C3DUBImage& image);
	~TVolumeRenderer();
	
	
	virtual const char* get_classname() const;
	virtual void get_classname_list(list<string> *classlist) const;
	
	void pass_transfer_table(const TransferTable& transfertable);		//passing a transfertable to the renderer

private:

	C3DUBImage _M_volumen_data; 			//the loaded volumetexture
	GLuint		volumetexid, transfertexid;	//textur id
	GLuint		texsize;			//size of the texture
	GLubyte*	volumetexture;			//the used volumetexture
	GLfloat*	transfertable;			//the used transfertable
	class TGLShader*	Shader;			//the shading programm

};
#endif

