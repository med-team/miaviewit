/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __selector_hh
#define __selector_hh

#include <viewit/drawable.hh>
class TArrowListGrow; 

#define SELECTOR_CLASSNAME "TSelector"

class TSelector: public TDrawable {
	class TSelectorData *data; 
public:
	TSelector(const string& name,TCamera *c,TArrowListGrow *select_list);
	~TSelector();
	
	virtual const char* get_classname()const;
	virtual void get_classname_list(list<string> *classlist)const;
	
protected:
	virtual bool do_handle_mouse_event(int button,int x, int y); 
	virtual bool do_handle_key_event(int key);
	virtual void do_gl_draw(const TCamera& c) const; 
	virtual void draw_decoration(const C2DBounds& viewport,const TGLText& writer);
};

#endif
