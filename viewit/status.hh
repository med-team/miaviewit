/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __status_hh
#define __status_hh


#include <libxml++/libxml++.h>
#include <string>
#include <iostream>
#include <sstream>



class TStatusNode {
public: 	
	TStatusNode(xmlpp::Node *node): 
		m_node(node)
	{
	}
	
	template <class Content>
	void add_child_with_content(const std::string& name,  const Content& value);
	
	template <class Content>
	void get_child_content(const std::string& name, Content *value, Content predef)const;
private:
	xmlpp::Node *m_node;
};

/*
class TEngineState {

public:
	TEngineState();
	TEngineState(xmlpp::DomParser& parser);
	~TEngineState();
	
	bool TEngineState::write(std::ostream& os);
	
	const TStatusNode get_document_root()const;
	TStatusNode get_document_root();
	
private:	
	xmlpp::Document m_state_document;
	xmlpp::Node *m_root; 
};
*/

// template implementation
template <class Content>
void TStatusNode::add_child_with_content(const std::string& name,  const Content& value)
{
	std::stringstream s; 
	s << value; 
	
	xmlpp::Element *child = m_node->add_child(name);
	child->set_child_text(s.str());
	
}

template <class Content>
void TStatusNode::get_child_content(const std::string& name, Content *value, Content predef)const
{
	xmlpp::NodeSet set = m_node->find(name);
	if (set.size() > 1) {
		std::cerr << "get_child_content found more then one definition " << name << std::endl
			  << " ignoring all but the first" <<std::endl;  
	}
	
	if (set.size() < 1) {
		*value = predef;
	}else {
		std::stringstream s(set[0]->get_content());
		s >> *value; 
	}
}


#endif
