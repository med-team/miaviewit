/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#include <viewit/coordaxes.hh>
#include <viewit/gltext.hh>

TCoordAxes::TCoordAxes(const string& name) 
	: TDrawable(name),__origin(0.0, 0.0, 0.0), do_draw_decoration(true)
{;}

TCoordAxes::TCoordAxes(const string& name, const C3DFVector& origin)
	: TDrawable(name),__origin(origin), do_draw_decoration(true)
{;}

TCoordAxes::~TCoordAxes()
{;}

const char* TCoordAxes::get_classname() const 
{
	return COORDAXES_CLASSNAME;
}

void TCoordAxes::get_classname_list(list<string> *classlist) const
{
	if (classlist) {
		classlist->push_front(COORDAXES_CLASSNAME);
		TDrawable::get_classname_list(classlist);
	}
}

void TCoordAxes::draw_decoration(const C2DBounds& viewport,const TGLText& writer)
{
	if ( !(do_draw_decoration && is_selected()) )
		return;  
		
	int y = viewport.y; 
	
	glColor4f(0.0f,.5f,0.0f,0.3f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glBegin(GL_QUADS);
	glVertex2f(5.0f,y - 10.0f);
	glVertex2f(90.0f,y - 10.0f);
	glVertex2f(90.0f,y - 110.0f);
	glVertex2f(5.0f,y - 110.0f);
	glEnd();
	glDisable(GL_BLEND);
	
	char value[20]; 
	
	snprintf(value,20,"x-axis");
	glColor3f(0.0, 0.8, 0.8);
	writer.write(25, int(y - 34), value);
	
	glColor3f(0.8, 0.0, 0.8);
	writer.write(25, int(y - 67), "y-axis");
	
	snprintf(value,20,"z-axis");
	glColor3f(0.8, 0.8, 0.0);
	writer.write(25 , int(y - 100), value);
}


void TCoordAxes::do_gl_draw(const TCamera& c) const
{
	// draw x,y,z axis

	float width = 256.0;
	
	// x-axis
	glBegin(GL_LINES);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(  0.0 ,__origin.y, __origin.z); 
	glVertex3f(width, __origin.y, __origin.z);
	glEnd();
	

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.0, 1.0, 1.0);
	glVertex3f(width,    __origin.y, __origin.z);
	glColor3f(0.0, 0.8, 0.8);
	glVertex3f(width-10.0, __origin.y-2.0, __origin.z-2);
	glColor3f(0.0, 0.6, 0.6);
	glVertex3f(width-10.0, __origin.y+2.0, __origin.z-2);
	glColor3f(0.0, 0.4, 0.4);
	glVertex3f(width-10.0, __origin.y+2.0, __origin.z+2);
	glColor3f(0.0, 0.6, 0.6);
	glVertex3f(width-10.0, __origin.y-2.0, __origin.z+2);
	glColor3f(0.0, 0.8, 0.8);
	glVertex3f(width-10.0, __origin.y-2.0, __origin.z-2);
	glEnd();
	
	// y-axis
	glBegin(GL_LINES);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(__origin.x,   0.0, __origin.z); 
	glVertex3f(__origin.x, width, __origin.z);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0, 0.0, 1.0);
	glVertex3f(__origin.x,   width,    __origin.z);
	glColor3f(0.8, 0.0, 0.8);
	glVertex3f(__origin.x-2, width-10, __origin.z-2);
	glColor3f(0.6, 0.0, 0.6);
	glVertex3f(__origin.x+2, width-10, __origin.z-2);
	glColor3f(0.4, 0.0, 0.4);
	glVertex3f(__origin.x+2, width-10, __origin.z+2);
	glColor3f(0.6, 0.0, 0.6);
	glVertex3f(__origin.x-2, width-10, __origin.z+2);
	glColor3f(0.8, 0.0, 0.8);
	glVertex3f(__origin.x-2, width-10, __origin.z-2);
	glEnd();
	

	// z-axis
	glBegin(GL_LINES);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(__origin.x, __origin.y,        0.0); 
	glVertex3f(__origin.x,     __origin.y,     width);
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0, 1.0, 0.0);
	glVertex3f(__origin.x,     __origin.y,     width);
	glColor3f(0.8, 0.8, 0.0);
	glVertex3f(__origin.x-2, __origin.y-2.0, width-10);
	glColor3f(0.6, 0.6, 0.0);
	glVertex3f(__origin.x+2, __origin.y-2.0, width-10);
	glColor3f(0.4, 0.4, 0.0);
	glVertex3f(__origin.x+0, __origin.y+2.0, width-10);
	glColor3f(0.6, 0.6, 0.0);
	glVertex3f(__origin.x-2, __origin.y+2.0, width-10);
	glColor3f(0.8, 0.8, 0.0);
	glVertex3f(__origin.x-2, __origin.y-2.0, width-10);
	glEnd();
}

bool TCoordAxes::do_handle_key_event(int key)
{
	switch (key) {
	case 't':
	case 'T':
	case '1':
	case '2':
	case '3':
	case '4': 
	case 'e': break; 
	case 'd':do_draw_decoration = !do_draw_decoration; break;		
	default: return TDrawable::do_handle_key_event(key); 
	}
	return true;
}

bool TCoordAxes::do_handle_command_event(PEventInfo info)
{
	switch (info->get_command()) {

	case EV_COORDAXES_SHOW_DECORATION:
		do_draw_decoration = !do_draw_decoration; 
		break;
		
	default: 
		return TDrawable::do_handle_command_event(info);
	}
	return true;	
}
