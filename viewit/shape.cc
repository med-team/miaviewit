/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include <GL/gl.h>
#include <GL/glu.h>

#include <stdexcept>
#include <iostream>

#include <viewit/glerror.hh>

#include <viewit/shape.hh>



void CShape::pre_draw()
{
}

void CShape::gl_attach()
{
}
	
void CShape::gl_detach()
{
}
	

CSphereShape::CSphereShape(float radius):
	m_radius(radius)
{
}


void CSphereShape::pre_draw()
{
	glScalef(m_radius, m_radius, m_radius);
}



void CDeformedShape::pre_draw()
{
	make_gl_op();
}
	
	
void CDeformedShape::gl_attach()
{
}

void CDeformedShape::gl_detach()
{
}


CEllipsoidShape::CEllipsoidShape(const  C3DFVector& scale, const C3DFVector& rot):
	m_scale(scale),
	m_rot(rot)
{
}
	
void CEllipsoidShape::make_gl_op()
{
	glScalef(m_scale.x, m_scale.y, m_scale.z);
	glRotatef(m_rot.x, 1.0,0.0,0.0);
	glRotatef(m_rot.y, 0.0,1.0,0.0);
	glRotatef(m_rot.z, 0.0,0.0,1.0);
}


CFreeShape::CFreeShape(const C3DFMatrix& defo)
{
	memset(m_defo,0,sizeof(m_defo));
	float *pdefo = m_defo;
	
	m_defo[0] = defo.x.x; 
	m_defo[1] = defo.x.y; 
	m_defo[2] = defo.x.z; 
	m_defo[3] = 0.0f; 

	m_defo[4] = defo.y.x; 
	m_defo[5] = defo.y.y; 
	m_defo[6] = defo.y.z; 
	m_defo[7] = 0.0f; 

	m_defo[8] = defo.z.x; 
	m_defo[9] = defo.z.y; 
	m_defo[10] = defo.z.z; 
	m_defo[11] = 0.0f; 
	
	m_defo[12] = 0.0f; 
	m_defo[13] = 0.0f; 
	m_defo[14] = 0.0f; 
	m_defo[15]= 1.0f;
}

void CFreeShape::make_gl_op()
{
	glMultMatrixf(m_defo);
}
