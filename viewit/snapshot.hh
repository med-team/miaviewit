/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#ifndef __shapshot_hh
#define __shapshot_hh


#include <mia/2d/vector.hh>

/// a class to make a snapshot of the current image and sent it to the saver queue
class CSnapshotClient {
public:
	CSnapshotClient(); 
	
	/// destruction is public
	~CSnapshotClient();
	
	/** takes a snapshot of the current OpenGL-backbuffer
	    \param camera describes the size of the image 
	*/
	void foto(const mia::C2DBounds& camera);
private:
	struct CSnapshotClientImpl *pimpl; 
};

#endif
