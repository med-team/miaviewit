/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __structures_hh
#define __structures_hh




#include <list>
struct vertex_t {
	C3DFVector vertex; 
};

struct normal_vertex_t {
	C3DFVector normal; 	
	C3DFVector vertex; 
};

struct color_normal_vertex_t {
	TColor color; 
	C3DFVector normal; 	
	C3DFVector vertex; 
};

struct triangle_ui_t {
	unsigned int a,b,c;
};

struct ushort_triangle_t {
	unsigned short a,b,c;
};

typedef std::list<normal_vertex_t> TNormalVertexList; 
#endif
