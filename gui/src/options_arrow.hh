/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPTIONS_ARROW_HH
#define OPTIONS_ARROW_HH

#include "options_drawable.hh"

class GUI_ArrowList : public GUI_Drawable {
private:
	
        class Gtk::Button *m_number_increase;
        class Gtk::Button *m_number_decrease;
        class Gtk::Button *m_length_increase;
        class Gtk::Button *m_length_decrease;
        // arrow flip button 
        class Gtk::Button *flip_arrow_button;
	
	void add_options();
	
	void on_m_number_increase_clicked();
        void on_m_number_decrease_clicked();
        void on_m_length_increase_clicked();
        void on_m_length_decrease_clicked();
        // arrow flip button 
        void on_flip_arrow_button_clicked();

public:
	GUI_ArrowList(Gtk::VBox *box);
	
	virtual void reset();
};


class GUI_ArrowListGrow : public GUI_ArrowList {
private:
	
	void add_options();

public:
	GUI_ArrowListGrow(Gtk::VBox *box);
	
	virtual void reset();
};




#endif
