/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _WINOPTIONS_HH
#  include "WinOptions_glade.hh"
#  define _WINOPTIONS_HH

#include <gtkmm/scale.h>
#include <functional>


class TDrawable;



class WinOptions : public WinOptions_glade
{  
private:
	TDrawable *		__object;
	P3DTransformation 	__vfield;
	
	
	/** deletes the actual property widgeds if neccessary */
	void clear_options();
	
protected:
	void on_button_close_clicked();
	bool on_WinOptions_delete_event(GdkEventAny *ev);

public:
	WinOptions();
	~WinOptions();
	
	void on_WinOptions_set_visibility(bool visible);
	void on_WinOptions_set_object(TDrawable *object, P3DTransformation vfield, Glib::RefPtr<Gdk::Pixbuf> icon);
	void on_WinOptions_set_vectorfield(P3DTransformation vfield, std::string vfname, Glib::RefPtr<Gdk::Pixbuf> icon);
	void on_WinOptions_set_viewer(Glib::RefPtr<Gdk::Pixbuf> icon);
};
#endif
