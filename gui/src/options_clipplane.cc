/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "options_clipplane.hh"

#include <gtkmm/label.h>
#include <gtkmm/frame.h>
#include <gtkmm/button.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/checkbutton.h>

#include <viewit/event.hh>

extern CEventQueue *pending_events;

GUI_ClipPlane::GUI_ClipPlane(Gtk::VBox *box, bool show_slicepos) 
	: GUI_Slice(box,false,false,show_slicepos)
{
	add_options();
}

void GUI_ClipPlane::add_options()
{
	m_invert = Gtk::manage(new class Gtk::Button("Invert Clipplane"));
	m_invert->set_flags(Gtk::CAN_FOCUS);
	m_invert->set_border_width(6);
	m_invert->set_relief(Gtk::RELIEF_NORMAL);
	
	Gtk::Label *clipplane_frame_label = Gtk::manage(new class Gtk::Label("Clipplane propperties"));
	clipplane_frame_label->set_alignment(0.5,0.5);
	clipplane_frame_label->set_padding(0,0);
	clipplane_frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	clipplane_frame_label->set_line_wrap(false);
	clipplane_frame_label->set_use_markup(false);
	clipplane_frame_label->set_selectable(false);
	
	Gtk::Frame *clipplane_frame = Gtk::manage(new class Gtk::Frame());
	clipplane_frame->set_border_width(10);
	clipplane_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	clipplane_frame->set_label_align(0,0.5);
	clipplane_frame->add(*m_invert);
	clipplane_frame->set_label_widget(*clipplane_frame_label);

	m_invert->show();
	clipplane_frame_label->show();
	clipplane_frame->show();
	
	this->pack_start(*clipplane_frame);
	m_invert->signal_clicked().connect(sigc::mem_fun(*this, &GUI_ClipPlane::on_m_invert_clicked));   
}

void GUI_ClipPlane::on_m_invert_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_CLIPPLANE_INVERT));
	pending_events->push(event);
}

///////////////////////////////////////////////////////////////////////////////
//
// class GUI_FreeClipPlane
//
///////////////////////////////////////////////////////////////////////////////

GUI_FreeClipPlane::GUI_FreeClipPlane(Gtk::VBox *box) 
	: GUI_ClipPlane(box,false)
{
	add_options();
}

void GUI_FreeClipPlane::add_options() 
{
	m_activate_tripod = Gtk::manage(new class Gtk::Button("activate"));
	m_activate_tripod->set_flags(Gtk::CAN_FOCUS);
	m_activate_tripod->set_relief(Gtk::RELIEF_NORMAL);
	
	Gtk::HBox  *tripod_box = Gtk::manage(new class Gtk::HBox(false, 0));
	tripod_box->set_border_width(8);
	tripod_box->pack_start(*m_activate_tripod, Gtk::PACK_EXPAND_WIDGET, 4);

	Gtk::Label *tripod_label = Gtk::manage(new class Gtk::Label("select clipplane basepoints"));
	tripod_label->set_alignment(0.5,0.5);
	tripod_label->set_padding(0,0);
	tripod_label->set_justify(Gtk::JUSTIFY_LEFT);
	tripod_label->set_line_wrap(false);
	tripod_label->set_use_markup(false);
	tripod_label->set_selectable(false);
	
	Gtk::Frame *tripod_frame = Gtk::manage(new class Gtk::Frame());
	tripod_frame->set_border_width(10);
	tripod_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	tripod_frame->set_label_align(0,0.5);
	tripod_frame->add(*tripod_box);
	tripod_frame->set_label_widget(*tripod_label);
	
	m_activate_tripod->show();
	tripod_box->show();
	tripod_label->show();
	tripod_frame->show();
	this->pack_start(*tripod_frame);
	
	m_activate_tripod->signal_clicked().connect(sigc::mem_fun(*this,&GUI_FreeClipPlane::on_m_activate_tripod_clicked));
}

void GUI_FreeClipPlane::on_m_activate_tripod_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_FREECLIPPLANE_ACTIVATE_TRIPOD));
	pending_events->push(event);
}

