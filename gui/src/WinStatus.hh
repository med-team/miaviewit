/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include <string>

#ifndef _WINSTATUS_HH
#  include "WinStatus_glade.hh"
#  define _WINSTATUS_HH



class WinStatus : public WinStatus_glade
{  
public:
	WinStatus();
	
	// signal handlers - userdefined
	virtual void on_WinStatus_set_visibility(bool visible);
	virtual void on_WinStatus_append_text(std::string &s);

protected:
	// signal handlers - standard
	bool on_WinStatus_delete_event(GdkEventAny *ev);
	
private:
	Glib::RefPtr<Gtk::TextBuffer::Mark> TextEndMark;
	
};
#endif
