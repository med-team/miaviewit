/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "options_coordaxes.hh"
#include <viewit/event.hh>
#include <gtkmm/label.h>
#include <gtkmm/frame.h>


extern CEventQueue *pending_events;

GUI_Coordaxes::GUI_Coordaxes(Gtk::VBox *box)
	: GUI_Drawable(box, false, false)
{
	add_options();
}
	
void GUI_Coordaxes::add_options()
{
	m_toggle_decoration = Gtk::manage(new class Gtk::Button("Toggle legend"));
	m_toggle_decoration->set_flags(Gtk::CAN_FOCUS);
	m_toggle_decoration->set_border_width(6);
	m_toggle_decoration->set_relief(Gtk::RELIEF_NORMAL);

	Gtk::Label *coordaxes_label = Gtk::manage(new class Gtk::Label("Coordaxes propperties"));
	coordaxes_label->set_alignment(0.5,0.5);
	coordaxes_label->set_padding(0,0);
	coordaxes_label->set_justify(Gtk::JUSTIFY_LEFT);
	coordaxes_label->set_line_wrap(false);
	coordaxes_label->set_use_markup(false);
	coordaxes_label->set_selectable(false);

	
	Gtk::Frame *coordaxes_frame = Gtk::manage(new class Gtk::Frame());
	coordaxes_frame->set_border_width(10);
	coordaxes_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	coordaxes_frame->set_label_align(0,0.5);
	coordaxes_frame->add(*m_toggle_decoration);
	coordaxes_frame->set_label_widget(*coordaxes_label);
	
	m_toggle_decoration->show();
	coordaxes_label->show();
	coordaxes_frame->show();
	this->pack_start(*coordaxes_frame);
	
	m_toggle_decoration->signal_clicked().connect(sigc::mem_fun(*this,&GUI_Coordaxes::on_m_toggle_decoration_clicked));	
}

void GUI_Coordaxes::reset()
{
	GUI_Drawable::reset();
}

void GUI_Coordaxes::on_m_toggle_decoration_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_COORDAXES_SHOW_DECORATION));
	pending_events->push(event);
}
