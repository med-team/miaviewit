/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"
#include "volume_options_glade.hh"
#include <gtkmm/curve.h>
#include <gtkmm/label.h>
#include <gtkmm/notebook.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/button.h>
#include <gtkmm/fixed.h>


volume_options_glade::volume_options_glade(
) : Gtk::Window(Gtk::WINDOW_TOPLEVEL)
{  
   Gtk::Window *volume_options = this;
   
   p_alphacurve = manage(new class Gtk::Curve());
   Gtk::Label *label1 = manage(new class Gtk::Label("Alpha"));
   p_redcurve = manage(new class Gtk::Curve());
   
   Gtk::Label *label2 = manage(new class Gtk::Label("Red"));
   p_greencurve = manage(new class Gtk::Curve());
   Gtk::Label *label3 = manage(new class Gtk::Label("Green"));
   p_bluecurve = manage(new class Gtk::Curve());

   Gtk::Label *label4 = manage(new class Gtk::Label("Blue"));
   Gtk::Notebook *notebook1 = manage(new class Gtk::Notebook());
   Gtk::Adjustment *spinbutton1_adj = manage(new class Gtk::Adjustment(50, 1, 256, 1, 10, 10));
   Gtk::SpinButton *spinbutton1 = manage(new class Gtk::SpinButton(*spinbutton1_adj, 1, 0));
   p_spinbutton = spinbutton1;
   Gtk::Adjustment *spinbutton2_adj = manage(new class Gtk::Adjustment(2, 0.1, 100, 0.1, 10, 10));
   Gtk::SpinButton *spinbutton2 = manage(new class Gtk::SpinButton(*spinbutton2_adj, 1, 2));
   p_slice_mult = spinbutton2;
   Gtk::Label *label6 = manage(new class Gtk::Label("SliceAmount"));
   Gtk::Label *label5 = manage(new class Gtk::Label("Threshold"));
   Gtk::Button *button1 = manage(new class Gtk::Button("Use"));
   Gtk::Fixed *fixed1 = manage(new class Gtk::Fixed());
   label1->set_alignment(0.5,0.5);
   label1->set_padding(0,0);
   label1->set_justify(Gtk::JUSTIFY_CENTER);
   label1->set_line_wrap(false);
   label2->set_alignment(0.5,0.5);
   label2->set_padding(0,0);
   label2->set_justify(Gtk::JUSTIFY_CENTER);
   label2->set_line_wrap(false);
   label3->set_alignment(0.5,0.5);
   label3->set_padding(0,0);
   label3->set_justify(Gtk::JUSTIFY_CENTER);
   label3->set_line_wrap(false);
   label4->set_alignment(0.5,0.5);
   label4->set_padding(0,0);
   label4->set_justify(Gtk::JUSTIFY_CENTER);
   label4->set_line_wrap(false);
   notebook1->set_size_request(400,256);
   notebook1->set_flags(Gtk::CAN_FOCUS);
   notebook1->set_show_tabs(true);
   notebook1->set_show_border(true);
   notebook1->set_tab_pos(Gtk::POS_TOP);
   notebook1->set_scrollable(false);
   notebook1->popup_disable();
   notebook1->pages().push_back(Gtk::Notebook_Helpers::TabElem(*p_alphacurve, *label1));
   notebook1->pages().push_back(Gtk::Notebook_Helpers::TabElem(*p_redcurve, *label2));
   notebook1->pages().push_back(Gtk::Notebook_Helpers::TabElem(*p_greencurve, *label3));
   notebook1->pages().push_back(Gtk::Notebook_Helpers::TabElem(*p_bluecurve, *label4));
   p_alphacurve->set_range(0,256,0,1);
   p_redcurve->set_range(0,256,0,1);
   p_greencurve->set_range(0,256,0,1);
   p_bluecurve->set_range(0,256,0,1);

#if 0   
   p_alphacurve->set_curve_type(Gtk::CURVE_TYPE_LINEAR);
   p_redcurve->set_curve_type(Gtk::CURVE_TYPE_LINEAR);
   p_greencurve->set_curve_type(Gtk::CURVE_TYPE_LINEAR);
   p_bluecurve->set_curve_type(Gtk::CURVE_TYPE_LINEAR);   
#endif

   
   spinbutton1->set_size_request(45,26);
   spinbutton1->set_flags(Gtk::CAN_FOCUS);
   spinbutton1->set_update_policy(Gtk::UPDATE_ALWAYS);
   spinbutton1->set_numeric(false);
   spinbutton1->set_digits(0);
   spinbutton1->set_wrap(false);
   spinbutton1->set_snap_to_ticks(false);
   spinbutton2->set_size_request(58,26);
   spinbutton2->set_flags(Gtk::CAN_FOCUS);
   spinbutton2->set_update_policy(Gtk::UPDATE_ALWAYS);
   spinbutton2->set_numeric(false);
   spinbutton2->set_digits(2);
   spinbutton2->set_wrap(false);
   spinbutton2->set_snap_to_ticks(false);
   label5->set_size_request(79,20);
   label5->set_alignment(0.5,0.5);
   label5->set_padding(0,0);
   label5->set_justify(Gtk::JUSTIFY_CENTER);
   label5->set_line_wrap(false);
   label6->set_size_request(94,20);
   label6->set_alignment(0.5,0.5);
   label6->set_padding(0,0);
   label6->set_justify(Gtk::JUSTIFY_CENTER);
   label6->set_line_wrap(false);
   //button1->set_usize(61,26);
   button1->set_flags(Gtk::CAN_FOCUS);
   button1->set_relief(Gtk::RELIEF_NORMAL);
   fixed1->put(*notebook1, 0, 0);
   fixed1->put(*spinbutton1, 120, 264);
   fixed1->put(*button1, 272, 296);
   fixed1->put(*label5, 8, 267);
   fixed1->put(*spinbutton2, 120, 296);
   fixed1->put(*label6, 16, 299);
   volume_options->set_title("Volumerenderer Options");
   volume_options->set_modal(false);
   volume_options->set_position(Gtk::WIN_POS_NONE);
   volume_options->add(*fixed1);
   p_alphacurve->show();
   label1->show();
   p_redcurve->show();
   label2->show();
   p_greencurve->show();
   label3->show();
   p_bluecurve->show();
   label4->show();
   notebook1->show();
   spinbutton1->show();
   label5->show();
   button1->show();
   spinbutton2->show();
   label6->show();
   fixed1->show();
   volume_options->show();
   button1->signal_clicked().connect(sigc::mem_fun(*this, &volume_options_glade::on_use_button_clicked));
}

volume_options_glade::~volume_options_glade()
{  
}
