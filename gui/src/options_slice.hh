/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPTIONS_SLICE_HH
#define OPTIONS_SLICE_HH

#include "options_drawable.hh"

class GUI_Slice : public GUI_Drawable {
private:
	class Gtk::HScale *m_slice;

	void add_slice_options(float value, float from, float to);
	
	void on_m_slice_value_changed();
	
public:
	GUI_Slice(Gtk::VBox *box);
	GUI_Slice(Gtk::VBox *box, bool has_clipping, bool has_transparency, bool has_position);
	
	virtual void reset();
};

#endif
