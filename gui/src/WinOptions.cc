/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"
#include "SignalHandler.hh"

#include "WinOptions.hh"
#include "userini.hh"

#include <gtkmm/adjustment.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/frame.h>

#include "optionsvbox.hh"
#include "vectorfieldvbox.hh"
#include "viewervbox.hh"

extern SignalHandler	*viewit_signal_handler;
extern XMLini		*inifile;


///////////////////////////////////////////////////////////////////////////////
//
// class WinOptions - construction and destruction
//
///////////////////////////////////////////////////////////////////////////////

//
// constructor
//
WinOptions::WinOptions() 
	: __object(NULL), __vfield(NULL)
{
	std::string section	= "WinOptions";
	std::string key		= "visible";
	std::string value	= "true";		
		
	inifile->get_value(section,key,value);
	this->on_WinOptions_set_visibility(value == "true");	
	
	
	// set the position for this widget
	key	    = "position";
	
	int width  = 0;
	int height = 560;
	
	if (inifile->get_value(section,key,value)) {
		unsigned int pos = value.find_first_of(' ');
		width  = atoi(std::string(value.substr(0,pos)).c_str());
		height = atoi(std::string(value.substr(pos+1,value.length())).c_str());
	}		
	this->move(width,height);
}
	

//
// constructor
//
WinOptions::~WinOptions()
{
	std::string section = "WinOptions";
	std::string key	= "position";
	std::string value;
	char buffer[255];
	int width,height;
	get_position(width,height);
	sprintf(buffer,"%d %d",width,height);
	value = buffer;
	inifile->set_value(section,key,value);	
}


///////////////////////////////////////////////////////////////////////////////
//
// class WinOptions - events and signal functions
//
///////////////////////////////////////////////////////////////////////////////

void WinOptions::on_WinOptions_set_visibility(bool visible)
{
	if (visible)
		this->show();
	else
		this->hide();
}


void WinOptions::on_button_close_clicked()
{
	this->hide();
	viewit_signal_handler->WinMain_hide_notification().emit("WinOptions");
}


bool WinOptions::on_WinOptions_delete_event(GdkEventAny *ev)
{
 	viewit_signal_handler->WinMain_hide_notification().emit("WinOptions");
	return false;
}

///////////////////////////////////////////////////////////////////////////////
//
// class WinOptions - drawable objects options
//
///////////////////////////////////////////////////////////////////////////////

void WinOptions::on_WinOptions_set_object(TDrawable *object, P3DTransformation vfield, Glib::RefPtr<Gdk::Pixbuf> icon) 
{
	// if the Window is initialized yet then clear it
	if (__options_frame->get_child()) __options_frame->remove();
	
	__object = object;
	__vfield = vfield;
	
	// if no valid object is given, then it's all done here
	if (!__object) return;
	
	__object_icon->set(icon);
	__object_name->set_text(object->get_name());

	TOptionsVBox *optionsvbox = new class TOptionsVBox(object);
	__options_frame->add(*Gtk::manage(dynamic_cast<Gtk::Widget *>(optionsvbox)));
	__button_reset->signal_clicked().connect(sigc::mem_fun(*optionsvbox, &TOptionsVBox::reset));
	__options_frame->show_all();
	this->resize(1,1);
}


///////////////////////////////////////////////////////////////////////////////
//
// class WinOptions - vectorfield options
//
///////////////////////////////////////////////////////////////////////////////

void WinOptions::on_WinOptions_set_vectorfield(P3DTransformation vfield, std::string vfname, Glib::RefPtr<Gdk::Pixbuf> icon) 
{
	// if the Window is initialized yet then clear it
	if (__options_frame->get_child()) __options_frame->remove();
	
	
	__vfield = vfield;
	__object_icon->set(icon);
	__object_name->set_text(vfname);
	
	// if no valid object is given, then it's all done here
	if (!__vfield) return;

  	Gtk::Widget *vectorfieldvbox = new class TVectorfieldVBox();
  	__options_frame->add(*vectorfieldvbox);
 	__options_frame->show_all();
	this->resize(1,1);
}

///////////////////////////////////////////////////////////////////////////////
//
// class WinOptions - glviewer options
//
///////////////////////////////////////////////////////////////////////////////

void WinOptions::on_WinOptions_set_viewer(Glib::RefPtr<Gdk::Pixbuf> icon)
{
	// if the Window is initialized yet then clear it
	if (__options_frame->get_child()) __options_frame->remove();
	__object_icon->set(icon);
	__object_name->set_text("Viewer options");
	
  	Gtk::Widget *viewervbox = new class TViewerVBox();
  	__options_frame->add(*viewervbox);
 	__options_frame->show_all();
	this->resize(1,1);
}
