/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"
#include "volume_options.hh"
#include <math.h>

#define TABLE_SIZE 256

void volume_options::on_use_button_clicked()
{  
	
	std::vector<GLfloat> alphalist(TABLE_SIZE);
	std::vector<GLfloat> redlist(TABLE_SIZE);	
	std::vector<GLfloat> bluelist(TABLE_SIZE);
	std::vector<GLfloat> greenlist(TABLE_SIZE);
	
	p_alphacurve->get_vector(alphalist.size(),&alphalist[0]);
	p_redcurve->get_vector(redlist.size(),&redlist[0]);
	p_greencurve->get_vector(greenlist.size(), &greenlist[0]);
	p_bluecurve->get_vector(bluelist.size(), &bluelist[0]);
	
	GLfloat thres = p_spinbutton->get_value()-1;	//getting the spinbutton-values
	GLfloat slice = p_slice_mult->get_value();
	
	TVolumeRenderer::TransferTable tt(TABLE_SIZE);
	TVolumeRenderer::TransferTable::iterator i = tt.begin();
	
	for(size_t k=0;k<tt.size();k++, ++i) 
	{
		*i = C4DFVector(redlist[k],
				greenlist[k],
				bluelist[k], 
				pow(alphalist[k], 2.0f));
		cerr << k << *i << endl; 
	
	}
	
	VolumeRenderer->pass_transfer_table(tt);	//passing values to the shader
	VolumeRenderer->slice_multiplier=slice;
	VolumeRenderer->threshold=0.0;
}

