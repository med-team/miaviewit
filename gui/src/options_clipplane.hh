/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPTIONS_CLIPPLANE_HH
#define OPTIONS_CLIPPLANE_HH

#include "options_slice.hh"
#include <gtkmm/checkbutton.h>

class GUI_ClipPlane : public GUI_Slice {
private:
	class Gtk::Button *	 m_invert;
	
	void add_options();
	
	void on_m_invert_clicked();
	
public:
	GUI_ClipPlane(Gtk::VBox *box, bool show_slicepos=true);
};

class GUI_FreeClipPlane : public GUI_ClipPlane {
private:
	class Gtk::Button *m_activate_tripod;

	void on_m_activate_tripod_clicked();
	
	void add_options();
	
public:
	GUI_FreeClipPlane(Gtk::VBox *box);
};

#endif
