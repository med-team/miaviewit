/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"
#include "WinStatus.hh"

#include "SignalHandler.hh"
#include "userini.hh"

#include <iostream>

extern SignalHandler	*viewit_signal_handler;
extern XMLini		*inifile;

WinStatus::WinStatus()
{
	std::string section	= "WinStatus";
	std::string key		= "visible";
	std::string value	= "";		
		
	inifile->get_value(section,key,value);
	this->on_WinStatus_set_visibility(value == "true");
	
	TextEndMark = status->get_buffer()->create_mark(status->get_buffer()->end());
}


bool WinStatus::on_WinStatus_delete_event(GdkEventAny *ev)
{  
	viewit_signal_handler->WinMain_hide_notification().emit("WinStatus");
	return false;
}

void WinStatus::on_WinStatus_set_visibility(bool visible)
{
	if (visible)
		this->show();
	else
		this->hide();
}

void WinStatus::on_WinStatus_append_text(std::string &s) 
{
	Glib::RefPtr<Gtk::TextBuffer> buffer = status->get_buffer();
	Gtk::TextBuffer::iterator ite = buffer->end();

	status->get_buffer()->move_mark(TextEndMark,ite);
	buffer->insert(ite,s);
	status->scroll_mark_onscreen(TextEndMark);
}	
	
