/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _VOLUME_OPTIONS_GLADE_HH
#  define _VOLUME_OPTIONS_GLADE_HH

#include <gtkmm/window.h>
#include <gtkmm/curve.h>
#include <gtkmm/spinbutton.h>

class volume_options_glade : public Gtk::Window
{  
public:
        
        class Gtk::Window *volume_options;
protected:
        class Gtk::Curve *p_alphacurve;
	class Gtk::Curve *p_redcurve;
	class Gtk::Curve *p_greencurve;
	class Gtk::Curve *p_bluecurve;
	class Gtk::SpinButton *p_spinbutton;
	class Gtk::SpinButton *p_slice_mult;
        volume_options_glade();
        
        ~volume_options_glade();
private:
        virtual void on_use_button_clicked() = 0;
};
#endif
