/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "optionsvbox.hh"

#include "options_drawable.hh"
#include "options_clipplane.hh"
#include "options_mesh.hh"
#include "options_arrow.hh"
#include "options_selector.hh"
#include "options_measure.hh"
#include "options_voxelcloud.hh"
#include "options_slice.hh"
#include "options_glslice.hh"
#include "options_cpdrawlist.hh"
#include "options_wireframe.hh"
#include "options_smarties.hh"
#include "options_coordaxes.hh"

#include <viewit/glmesh.hh>
#include <viewit/clip_plane.hh>
#include <viewit/glimage.hh>
#include "viewit/glmesh.hh"
#include "viewit/arrow.hh"
#include "viewit/selector.hh"
#include "viewit/measure.hh"
#include "viewit/voxelmove.hh"
#include "viewit/cp_draw.hh"
#include "viewit/wireframe.hh"
#include "viewit/smarty.hh"
#include "viewit/coordaxes.hh"

#include <gtkmm/label.h>

TOptionsVBox::TOptionsVBox(TDrawable *object)
	:__widget(NULL)
{
	if (object) {
		
		std::string classname = object->get_classname();
		
		if (classname == DRAWABLE_CLASSNAME) {
			__widget = new class GUI_Drawable(this);
		} else if (classname == GLMESH_CLASSNAME) {
			__widget = new class GUI_Mesh(this);
		} else if (classname == CLIPPLANE_CLASSNAME) {
			__widget = new class GUI_ClipPlane(this);
		} else if (classname == FREECLIPPLANE_CLASSNAME) {
 			__widget = new class GUI_FreeClipPlane(this);
		} else if (classname == ARROWLIST_CLASSNAME) {
			__widget = new class GUI_ArrowList(this);
		} else if (classname == ARROWLISTGROW_CLASSNAME) {
			__widget = new class GUI_ArrowListGrow(this);
		} else if (classname == SELECTOR_CLASSNAME) {
			__widget = new class GUI_Selector(this);
		} else if (classname == MEASURE_CLASSNAME) {
			__widget = new class GUI_Measure(this);
		} else if (classname == VOXELCLOUD_CLASSNAME) {
			__widget = new class GUI_VoxelCloud(this);
		} else if (classname == GLSLICE_CLASSNAME) {
			__widget = new class GUI_GLSlice(this);
		} else if (classname == CPDRAWLIST_CLASSNAME) {
 			__widget = new class GUI_CPDrawList(this);
 		} else if (classname == WIREFRAME_CLASSNAME) {
 			__widget = new class GUI_Wireframe(this);
 		} else if (classname == SMARTIES_CLASSNAME) {
			__widget = new class GUI_Smarties(this);
		} else if (classname == COORDAXES_CLASSNAME) {
			__widget = new class GUI_Coordaxes(this);
		}
	}
		
	if (!__widget) {
		std::cout << "TOptionsFrame::TOptionsFrame: unknown object\n" << std::flush;
		this->pack_start(*Gtk::manage(new class Gtk::Label("No options available")));
	} 
	this->show_all();
}
	
void TOptionsVBox::reset()
{
	__widget->reset();
}
