/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __WinDialogs_hh
#define __WinDialogs_hh

#include <gtkmm/window.h>
#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include <gtkmm/liststore.h>
#include <gtkmm/fileselection.h>

/************************************************************************************************************************
 *															*
 *							FileSelectionDialog						*
 *															*
 ***********************************************************************************************************************/

class FileSelectionDialog : public Gtk::FileSelection
{  
private:
	FileSelectionDialog(const std::string title);
	
	std::list<std::string> *__filelist;
	static std::string __path;
	
protected:
	virtual void on_cancel_clicked();
        virtual void on_ok_clicked();

public:
	static std::list<std::string>*	create_FilesSelectionDialog(const std::string title);
	static std::string		create_FileSelectionDialog(const std::string title);
	static std::string&		path();
};


/************************************************************************************************************************
 *															*
 *							StringInputDialog						*
 *															*
 ***********************************************************************************************************************/

class StringInputDialog : public Gtk::Dialog
{
protected:
	StringInputDialog(const std::string title, const std::string default_text);
	Gtk::Entry *m_string_entry;
	
public:
	static std::string create_StringInputDialog(const std::string title, const std::string default_text);
};


/************************************************************************************************************************
 *															*
 *							StringListDialog						*
 *															*
 ***********************************************************************************************************************/


class StringListDialog : public Gtk::Dialog
{  
private:
	void on_add_clicked();
        void on_update_clicked();
        void on_delete_clicked();
        void on_clear_clicked();
        void on_ok_clicked();
        void on_cancel_clicked();
	void on_row_changed();
	
	StringListDialog(const std::string title, std::list<std::string> *list);

protected:
	//Treeview model columns:
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns() { add(m_col_string); }
		Gtk::TreeModelColumn<std::string> m_col_string;
	};
	

	ModelColumns			m_columns;
	class Gtk::TreeView *		m_listview;
	Glib::RefPtr<Gtk::ListStore>	m_refTreeModel;
	
	class Gtk::Entry *		m_textentry;
	std::list<std::string> *	stringlist;
	

	
public:
	static bool create_StringListDialog(const std::string title, std::list<std::string> *list);
};

#endif

