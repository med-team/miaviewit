/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef PROGRESS_WINDOW_HH
#define PROGRESS_WINDOW_HH

#include <mia/core/callback.hh>

class CLoadProgressImpl;

class CLoadProgress: public mia::CProgressCallback {
public:	
	CLoadProgress();
	~CLoadProgress();
	
private:
	virtual void do_update(int step);
	virtual void do_set_range(int range); 
	virtual void do_pulse();

	CLoadProgressImpl *impl; 
	
};

#endif
