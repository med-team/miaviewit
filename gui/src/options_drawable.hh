/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPTIONS_DRAWABLE_HH
#define OPTIONS_DRAWABLE_HH

#include <gtkmm/scale.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/box.h>

class GUI_Drawable : public Gtk::VBox {
private:
	// transparency
	class Gtk::HScale *m_transparency;

	// clipping 
        class Gtk::ToggleButton *m_clipxy;
        class Gtk::ToggleButton *m_clipxz;
        class Gtk::ToggleButton *m_clipyz;
	class Gtk::ToggleButton *m_clipfree;

	void add_transparency_options(float transparency);
	void add_clipping_options(bool clip_xy, bool clip_xz, bool clip_yz, bool clip_free);
	
	void on_m_transparency_value_changed();
	void on_m_clipxy_clicked();
	void on_m_clipxz_clicked();
	void on_m_clipyz_clicked();
	void on_m_clipfree_clicked();
	
public:
	GUI_Drawable(Gtk::VBox *box);
	GUI_Drawable(Gtk::VBox *box, bool has_clipping, bool has_transparency);
	
	float get_transparency();
	
	virtual void reset();
};

#endif
