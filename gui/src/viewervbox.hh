/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef VIEWERVBOX_HH
#define VIEWERSVBOX_HH

#include <gtkmm/scale.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/image.h>
#include <gtkmm/togglebutton.h>

class TViewerVBox : public Gtk::VBox {
private:
	// views options
	class Gtk::Button *		m_next_view;
        class Gtk::Button *		m_save_view;
        class Gtk::Button *		m_save_all_views;

	// snapshot options
	Glib::RefPtr<Gdk::Pixbuf>	m_pixbuf_recording;
	Glib::RefPtr<Gdk::Pixbuf>	m_pixbuf_stop;
	
	class Gtk::Button *		m_snapshot;
        class Gtk::Button *		m_snapshot_all_views;
	class Gtk::Image *		m_movie_image;
	class Gtk::ToggleButton *	m_toggle_movie;
	
	// rotation speed options
	class Gtk::HScale *		m_rotation;
	
	// background brightness options
	class Gtk::HScale *		m_brightness;

	void add_views_options();
	void add_snapshot_options(bool snapshot_available, bool in_moviemode);
	void add_rotation_options(float speed);
	void add_brightness_options(float brightness);
	
	void on_m_next_view_clicked();
	void on_m_save_view_clicked();
	void on_m_save_all_views_clicked();
	void on_m_snapshot_clicked();
	void on_m_toggle_movie_clicked();
	void on_m_snapshot_all_views_clicked();
	void on_m_rotation_value_changed();
	void on_m_brightness_value_changed();

public:
	TViewerVBox();
	
	void reset();
};

#endif
