/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "options_slice.hh"
#include <viewit/event.hh>

#include <gtkmm/adjustment.h>
#include <gtkmm/frame.h>
#include <gtkmm/label.h>

extern CEventQueue *pending_events;

GUI_Slice::GUI_Slice(Gtk::VBox *box) 
	: GUI_Drawable(box), m_slice(NULL)
{
	event_t event;
	event.what = event_t::ev_command; 
	TSliceEventInfo *info = new TSliceEventInfo(EV_SLICE_GET_STATE);
	event.info.reset(info);
	pending_events->push(event);
	pending_events->handle_events();

	float from,to;
	info->get_slice_range(from,to);
	add_slice_options(info->get_slice_nr(), from, to);
}
	

GUI_Slice::GUI_Slice(Gtk::VBox *box, bool has_clipping, bool has_transparency, bool has_slice)
	: GUI_Drawable(box,has_clipping,has_transparency), m_slice(NULL)
{
	if (has_slice) {
		event_t event;
		event.what = event_t::ev_command; 
		TSliceEventInfo * info = new TSliceEventInfo(EV_SLICE_GET_STATE);
		event.info.reset(info);
		pending_events->push(event);
		pending_events->handle_events();
		
		float from,to;
		info->get_slice_range(from,to);
		add_slice_options(info->get_slice_nr(), from, to);
	}
}
	
void GUI_Slice::add_slice_options(float value, float from, float to)
{
	Gtk::Adjustment *slice_adj = Gtk::manage(new class Gtk::Adjustment(value, from, to-1.0, 1.0, 10.0, 0.0));
	m_slice = Gtk::manage(new class Gtk::HScale(*slice_adj));
	m_slice->set_flags(Gtk::CAN_FOCUS);
	m_slice->set_update_policy(Gtk::UPDATE_CONTINUOUS);
	m_slice->set_digits(1);
	m_slice->set_draw_value(true);
	m_slice->set_value_pos(Gtk::POS_TOP);
	
	Gtk::Label *slice_label = Gtk::manage(new class Gtk::Label("Position"));
	slice_label->set_alignment(0.5,0.5);
	slice_label->set_padding(0,0);
	slice_label->set_justify(Gtk::JUSTIFY_LEFT);
	slice_label->set_line_wrap(false);
	slice_label->set_use_markup(false);
	slice_label->set_selectable(false);
	
	Gtk::Frame *slice_frame = Gtk::manage(new class Gtk::Frame());
	slice_frame->set_border_width(10);
	slice_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	slice_frame->set_label_align(0,0.5);
	slice_frame->add(*m_slice);
	slice_frame->set_label_widget(*slice_label);
	
	m_slice->show();
	slice_label->show();
	slice_frame->show();
	this->pack_start(*slice_frame);
	
	m_slice->signal_value_changed().connect(sigc::mem_fun(*this,&GUI_Slice::on_m_slice_value_changed));
}

void GUI_Slice::on_m_slice_value_changed()
{
	event_t event;
	event.what = event_t::ev_command; 
	TSliceEventInfo *info = new TSliceEventInfo(EV_SLICE_SET_SLICE);
	event.info.reset(info);
	info->set_slice_nr(m_slice->get_value());
	pending_events->push(event);
}

void GUI_Slice::reset()
{
	GUI_Drawable::reset();
	
	if (m_slice) {
		m_slice->set_value((m_slice->get_adjustment()->get_upper() + 1.0) / 2.0);
	}
}
