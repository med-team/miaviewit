/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __SIGNALHANDLER_HH
#define __SIGNALHANDLER_HH


#include <sigc++/sigc++.h>
#include <gtkmm/image.h>

#include <viewit/cp_draw.hh>
#include <viewit/voxelmove.hh>



class TDrawable;

enum ViewitKeyTypes { VIEWIT_KEY_SNAPSHOT_SERIES	= 'C',	// do a series of snapshots using the standart views. 
		      VIEWIT_KEY_SNAPSHOT		= 'c',	// save snapshot
		      VIEWIT_KEY_SWITCH_BG_COLOR_INC    = 'B',	// switch background color increase
		      VIEWIT_KEY_SWITCH_BG_COLOR_DEC	= 'b',	// switch background color decrease
		      VIEWIT_KEY_AUTO_ROTATION		= 'r',	// toogle automatic rotation
		      VIEWIT_KEY_TOGGLE_VISIBILITY	= 'v'	// toogle the visibility of the selected object
};



struct ViewitObjects {
	
	ViewitObjects() : cloud(NULL), field(NULL), field_name(""), alpha(NULL), cplist(NULL)
	{;}
	
	TVoxelCloud *				cloud;		// voxelcloud
	
	P3DTransformation 			field;		// vectorfield 
	std::string				field_name;	// vectorfield name

	T2DAlphaSphere *			alpha;
	
	TCPDrawList *				cplist;		// critical point list
};




class SignalHandler
{
public:
	//
	// signal accessors for WinStatus
	//
	
	typedef sigc::signal1<void, bool>				type_signal_WinStatus_set_visibility;
	type_signal_WinStatus_set_visibility				WinStatus_set_visibility();
	typedef sigc::signal1<void, std::string & >			type_signal_WinStatus_append_text;
	type_signal_WinStatus_append_text				WinStatus_append_text();
	
	//
	// signal accessors for WinScene
	//

	typedef sigc::signal0<void>						type_signal_WinScene_detach;
	type_signal_WinScene_detach						WinScene_detach();
	
	typedef sigc::signal2<void, unsigned int, unsigned int>			type_signal_WinScene_resize;
	type_signal_WinScene_resize						WinScene_resize();
	
	typedef sigc::signal1<void, ViewitKeyTypes>				type_signal_WinScene_press_key;
	type_signal_WinScene_press_key						WinScene_press_key();
	
	typedef sigc::signal1<void, bool>					type_signal_WinScene_set_visibility;
	type_signal_WinScene_set_visibility					WinScene_set_visibility();
	
	//
	// signal accessors for WinMain
	//
	
	typedef sigc::signal1<void,std::string>					type_signal_WinMain_hide_notification;
	type_signal_WinMain_hide_notification					WinMain_hide_notification();
	
	typedef sigc::signal0<void>						type_signal_WinMain_objectmap_changed;
	type_signal_WinMain_objectmap_changed					WinMain_objectmap_changed() const;

	//
	// signal accessors for WinOptions
	//

	typedef sigc::signal1<void, bool>							type_signal_WinOptions_set_visibility;
	type_signal_WinOptions_set_visibility							WinOptions_set_visibility();

	typedef sigc::signal3<void, TDrawable*, P3DTransformation, Glib::RefPtr<Gdk::Pixbuf> >	type_signal_WinOptions_set_object;
	type_signal_WinOptions_set_object							WinOptions_set_object();

	typedef sigc::signal3<void, P3DTransformation, std::string, Glib::RefPtr<Gdk::Pixbuf> >	type_signal_WinOptions_set_vectorfield;
	type_signal_WinOptions_set_vectorfield							WinOptions_set_vectorfield();

	typedef sigc::signal1<void, Glib::RefPtr<Gdk::Pixbuf> >					type_signal_WinOptions_set_viewer;
	type_signal_WinOptions_set_viewer							WinOptions_set_viewer();
	
	//
	// constructor and destructor
	// 
	
	SignalHandler();
	
	virtual ~SignalHandler() throw();

	
protected:
	// signal accessors
	type_signal_WinStatus_set_visibility				m_signal_WinStatus_set_visibility;
	type_signal_WinStatus_append_text				m_signal_WinStatus_append_text;
	
	type_signal_WinScene_detach					m_signal_WinScene_detach;
	type_signal_WinScene_resize					m_signal_WinScene_resize;
	type_signal_WinScene_press_key					m_signal_WinScene_press_key;
	type_signal_WinScene_set_visibility				m_signal_WinScene_set_visibility;

	type_signal_WinMain_hide_notification				m_signal_WinMain_hide_notification;
	type_signal_WinMain_objectmap_changed				m_signal_WinMain_objectmap_changed;
	
	type_signal_WinOptions_set_visibility				m_signal_WinOptions_set_visibility;
	type_signal_WinOptions_set_object				m_signal_WinOptions_set_object;
	type_signal_WinOptions_set_vectorfield				m_signal_WinOptions_set_vectorfield;
	type_signal_WinOptions_set_viewer				m_signal_WinOptions_set_viewer;
private: 
	virtual void show_message(const std::string& message); 
};



#endif
