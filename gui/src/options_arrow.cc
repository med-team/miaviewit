/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "options_arrow.hh"
#include <viewit/event.hh>

#include <gtkmm/label.h>
#include <gtkmm/frame.h>
#include <gtkmm/image.h>
#include <gtkmm/alignment.h>

extern CEventQueue *pending_events;

GUI_ArrowList::GUI_ArrowList(Gtk::VBox *box) 
	: GUI_Drawable(box,true,false), m_number_increase(NULL), m_number_decrease(NULL),
	  m_length_increase(NULL), m_length_decrease(NULL), flip_arrow_button(NULL)
{
	add_options();
}
	
void GUI_ArrowList::add_options()
{
	Gtk::Image *increase_image = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-go-up"), Gtk::IconSize(4)));
	increase_image->set_alignment(0.5,0.5);
	increase_image->set_padding(0,0);

	Gtk::Label *number_incr_label = Gtk::manage(new class Gtk::Label("Arrow number"));
	number_incr_label->set_alignment(0.5,0.5);
	number_incr_label->set_padding(0,0);
	number_incr_label->set_justify(Gtk::JUSTIFY_LEFT);
	number_incr_label->set_line_wrap(false);
	number_incr_label->set_use_markup(false);
	number_incr_label->set_selectable(false);

	Gtk::HBox *number_incr_hbox = Gtk::manage(new class Gtk::HBox(false, 2));
	number_incr_hbox->pack_start(*increase_image, Gtk::PACK_SHRINK, 0);
	number_incr_hbox->pack_start(*number_incr_label, Gtk::PACK_SHRINK, 0);

	Gtk::Alignment *number_incr_alignment = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
	number_incr_alignment->add(*number_incr_hbox);
	
	m_number_increase = Gtk::manage(new class Gtk::Button());
	m_number_increase->set_flags(Gtk::CAN_FOCUS);
	m_number_increase->set_relief(Gtk::RELIEF_NORMAL);
	m_number_increase->add(*number_incr_alignment);
	
	Gtk::Image *decrease_image = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-go-down"), Gtk::IconSize(4)));
	decrease_image->set_alignment(0.5,0.5);
	decrease_image->set_padding(0,0);

	Gtk::Label *number_decr_label = Gtk::manage(new class Gtk::Label("Arrow number"));
	number_decr_label->set_alignment(0.5,0.5);
	number_decr_label->set_padding(0,0);
	number_decr_label->set_justify(Gtk::JUSTIFY_LEFT);
	number_decr_label->set_line_wrap(false);
	number_decr_label->set_use_markup(false);
	number_decr_label->set_selectable(false);
	
	Gtk::HBox *number_decr_hbox = Gtk::manage(new class Gtk::HBox(false, 2));
	number_decr_hbox->pack_start(*decrease_image, Gtk::PACK_SHRINK, 0);
	number_decr_hbox->pack_start(*number_decr_label, Gtk::PACK_SHRINK, 0);
	
	Gtk::Alignment *number_decr_alignment = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
	number_decr_alignment->add(*number_decr_hbox);
	
	m_number_decrease = Gtk::manage(new class Gtk::Button());
	m_number_decrease->set_flags(Gtk::CAN_FOCUS);
	m_number_decrease->set_relief(Gtk::RELIEF_NORMAL);
	m_number_decrease->add(*number_decr_alignment);
	
	Gtk::VBox *number_vbox = Gtk::manage(new class Gtk::VBox(false, 0));
	number_vbox->set_border_width(5);
	number_vbox->pack_start(*m_number_increase, Gtk::PACK_SHRINK, 0);
	number_vbox->pack_start(*m_number_decrease, Gtk::PACK_SHRINK, 0);
	
	Gtk::Image *incr_image = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-go-up"), Gtk::IconSize(4)));
	incr_image->set_alignment(0.5,0.5);
	incr_image->set_padding(0,0);
	
	Gtk::Label *length_incr_label = Gtk::manage(new class Gtk::Label("Arrow length"));
	length_incr_label->set_alignment(0.5,0.5);
	length_incr_label->set_padding(0,0);
	length_incr_label->set_justify(Gtk::JUSTIFY_LEFT);
	length_incr_label->set_line_wrap(false);
	length_incr_label->set_use_markup(false);
	length_incr_label->set_selectable(false);
	
	Gtk::HBox *length_incr_hbox = Gtk::manage(new class Gtk::HBox(false, 2));
	length_incr_hbox->pack_start(*incr_image, Gtk::PACK_SHRINK, 0);
	length_incr_hbox->pack_start(*length_incr_label, Gtk::PACK_SHRINK, 0);
	
	Gtk::Alignment *length_incr_alignment = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
	length_incr_alignment->add(*length_incr_hbox);
	
	m_length_increase = Gtk::manage(new class Gtk::Button());
	m_length_increase->set_flags(Gtk::CAN_FOCUS);
	m_length_increase->set_relief(Gtk::RELIEF_NORMAL);
	m_length_increase->add(*length_incr_alignment);
	
	Gtk::Image *decr_image = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-go-down"), Gtk::IconSize(4)));
	decr_image->set_alignment(0.5,0.5);
	decr_image->set_padding(0,0);

	Gtk::Label *length_decr_label = Gtk::manage(new class Gtk::Label("Arrow length"));
	length_decr_label->set_alignment(0.5,0.5);
	length_decr_label->set_padding(0,0);
	length_decr_label->set_justify(Gtk::JUSTIFY_LEFT);
	length_decr_label->set_line_wrap(false);
	length_decr_label->set_use_markup(false);
	length_decr_label->set_selectable(false);

	Gtk::HBox *length_decr_hbox = Gtk::manage(new class Gtk::HBox(false, 2));
	length_decr_hbox->pack_start(*decr_image, Gtk::PACK_SHRINK, 0);
	length_decr_hbox->pack_start(*length_decr_label, Gtk::PACK_SHRINK, 0);

	Gtk::Alignment *length_decr_alignment = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
	length_decr_alignment->add(*length_decr_hbox);
   
	m_length_decrease = Gtk::manage(new class Gtk::Button());
	m_length_decrease->set_flags(Gtk::CAN_FOCUS);
	m_length_decrease->set_relief(Gtk::RELIEF_NORMAL);
	m_length_decrease->add(*length_decr_alignment);
   
	Gtk::VBox *length_vbox = Gtk::manage(new class Gtk::VBox(false, 0));
	length_vbox->set_border_width(5);
	length_vbox->pack_start(*m_length_increase, Gtk::PACK_SHRINK, 0);
	length_vbox->pack_start(*m_length_decrease, Gtk::PACK_SHRINK, 0);

	Gtk::HBox *button_hbox = Gtk::manage(new class Gtk::HBox(false, 0));
	button_hbox->set_border_width(4);
	button_hbox->pack_start(*number_vbox);
	button_hbox->pack_start(*length_vbox);

	Gtk::Label *arrowlist_frame_label = Gtk::manage(new class Gtk::Label("Arrowlist properties"));
	arrowlist_frame_label->set_alignment(0.5,0.5);
	arrowlist_frame_label->set_padding(0,0);
	arrowlist_frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	arrowlist_frame_label->set_line_wrap(false);
	arrowlist_frame_label->set_use_markup(false);
	arrowlist_frame_label->set_selectable(false);
   
	Gtk::Frame *arrowlist_frame = Gtk::manage(new class Gtk::Frame());
	arrowlist_frame->set_border_width(10);
	arrowlist_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	arrowlist_frame->set_label_align(0,0.5);
	arrowlist_frame->add(*button_hbox);
	arrowlist_frame->set_label_widget(*arrowlist_frame_label);
   
	increase_image->show();
	number_incr_label->show();
	number_incr_hbox->show();
	number_incr_alignment->show();
	m_number_increase->show();
	decrease_image->show();
	number_decr_label->show();
	number_decr_hbox->show();
	number_decr_alignment->show();
	m_number_decrease->show();
	number_vbox->show();
	incr_image->show();
	length_incr_label->show();
	length_incr_hbox->show();
	length_incr_alignment->show();
	m_length_increase->show();
	decr_image->show();
	length_decr_label->show();
	length_decr_hbox->show();
	length_decr_alignment->show();
	m_length_decrease->show();
	length_vbox->show();
	button_hbox->show();
	arrowlist_frame_label->show();
	arrowlist_frame->show();	
	this->pack_start(*arrowlist_frame);
	
	// special options
	Gtk::Label *flip_arrow_label = Gtk::manage(new class Gtk::Label("Flip arrows"));
	flip_arrow_label->set_alignment(0.5,0.5);
	flip_arrow_label->set_padding(0,0);
	flip_arrow_label->set_justify(Gtk::JUSTIFY_LEFT);
	flip_arrow_label->set_line_wrap(false);
	flip_arrow_label->set_use_markup(false);
	flip_arrow_label->set_selectable(false);

	flip_arrow_button = Gtk::manage(new class Gtk::Button());
	flip_arrow_button->set_flags(Gtk::CAN_FOCUS);
	flip_arrow_button->set_relief(Gtk::RELIEF_NORMAL);
	//	flip_arrow_button->add(*special_alignment);
	flip_arrow_button->add(*flip_arrow_label);

	Gtk::HBox *special_hbox = Gtk::manage(new class Gtk::HBox(false, 1));
	//number_incr_hbox->pack_start(*increase_image, Gtk::PACK_SHRINK, 0);
	special_hbox->pack_start(*flip_arrow_button, Gtk::PACK_SHRINK, 0);
	special_hbox->set_border_width(5);

	Gtk::Label *special_frame_label = Gtk::manage(new class Gtk::Label("Transformer"));
	special_frame_label->set_alignment(0.5,0.5);
	special_frame_label->set_padding(0,0);
	special_frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	special_frame_label->set_line_wrap(false);
	special_frame_label->set_use_markup(false);
	special_frame_label->set_selectable(false);
   
	Gtk::Frame *special_frame = Gtk::manage(new class Gtk::Frame());
	special_frame->set_border_width(10);
	special_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	special_frame->set_label_align(0,0.5);
	special_frame->add(*special_hbox);
	special_frame->set_label_widget(*special_frame_label);

	flip_arrow_label->show();
	flip_arrow_button->show();
	special_hbox->show();
	special_frame_label->show();
	special_frame->show();	
   	this->pack_start(*special_frame);

	m_number_increase->signal_clicked().connect(sigc::mem_fun(*this, &GUI_ArrowList::on_m_number_increase_clicked));   
	m_number_decrease->signal_clicked().connect(sigc::mem_fun(*this, &GUI_ArrowList::on_m_number_decrease_clicked));   
	m_length_increase->signal_clicked().connect(sigc::mem_fun(*this, &GUI_ArrowList::on_m_length_increase_clicked));   
	m_length_decrease->signal_clicked().connect(sigc::mem_fun(*this, &GUI_ArrowList::on_m_length_decrease_clicked));   
	// flip arrow button
	flip_arrow_button->signal_clicked().connect(sigc::mem_fun(*this, &GUI_ArrowList::on_flip_arrow_button_clicked));   
}

void GUI_ArrowList::on_m_number_increase_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_ARROWLIST_INCREASE_ARROW_NUMBER));
	pending_events->push(event);
}

void GUI_ArrowList::on_m_number_decrease_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_ARROWLIST_DECREASE_ARROW_NUMBER));
	pending_events->push(event);
}

void GUI_ArrowList::on_m_length_increase_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_ARROWLIST_INCREASE_ARROW_LENGTH));
	pending_events->push(event);
}

void GUI_ArrowList::on_m_length_decrease_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_ARROWLIST_DECREASE_ARROW_LENGTH));
	pending_events->push(event);
}

// flip arrow button
void GUI_ArrowList::on_flip_arrow_button_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_ARROWLIST_FLIP_ARROWS));
	pending_events->push(event);
}


void GUI_ArrowList::reset()
{
	GUI_Drawable::reset();
}

GUI_ArrowListGrow::GUI_ArrowListGrow(Gtk::VBox *box) 
	: GUI_ArrowList(box)
{
	add_options();
}

void GUI_ArrowListGrow::add_options()
{;}

void GUI_ArrowListGrow::reset()
{
	GUI_ArrowList::reset();
}
