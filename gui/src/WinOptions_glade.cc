/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#if defined __GNUC__ && __GNUC__ < 3
#error This program will crash if compiled with g++ 2.x
// see the dynamic_cast bug in the gtkmm FAQ
#endif //
#include "config.h"
/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (GETTEXT_PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif
#include "WinOptions_glade.hh"
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include "../pixmaps/unknown32.xpm"
#include <gtkmm/box.h>
#include <gtkmm/frame.h>

WinOptions_glade::WinOptions_glade(
) : Gtk::Window(Gtk::WINDOW_TOPLEVEL)
{  
   
   Gtk::Window *WinOptions = this;
   gmm_data = new GlademmData(get_accel_group());
   
   Glib::RefPtr<Gdk::Bitmap> ___object_icon_mask;
   Glib::RefPtr<Gdk::Pixmap> ___object_icon_pixmap = Gdk::Pixmap::create_from_xpm(get_default_colormap(), ___object_icon_mask, unknown32_xpm);
   __object_icon = Gtk::manage(new class Gtk::Image(___object_icon_pixmap, ___object_icon_mask));
   __object_name = Gtk::manage(new class Gtk::Label(_("Object Name")));
   
   Gtk::HBox *hbox10 = Gtk::manage(new class Gtk::HBox(false, 0));
   Gtk::Frame *frame1 = Gtk::manage(new class Gtk::Frame());
   __options_frame = Gtk::manage(new class Gtk::Frame());
   
   Gtk::Frame *frame4 = Gtk::manage(new class Gtk::Frame());
   __button_reset = Gtk::manage(new class Gtk::Button(_("Reset")));
   __button_close = Gtk::manage(new class Gtk::Button(_("Close")));
   
   Gtk::HBox *hbox11 = Gtk::manage(new class Gtk::HBox(false, 0));
   Gtk::Frame *frame2 = Gtk::manage(new class Gtk::Frame());
   Gtk::VBox *vbox2 = Gtk::manage(new class Gtk::VBox(false, 0));
   __object_icon->set_alignment(0.5,0.5);
   __object_icon->set_padding(0,0);
   __object_name->set_alignment(0.5,0.5);
   __object_name->set_padding(0,0);
   __object_name->set_justify(Gtk::JUSTIFY_LEFT);
   __object_name->set_line_wrap(false);
   __object_name->set_use_markup(false);
   __object_name->set_selectable(false);
   hbox10->set_border_width(4);
   hbox10->pack_start(*__object_icon, Gtk::PACK_SHRINK, 0);
   hbox10->pack_start(*__object_name, Gtk::PACK_SHRINK, 8);
   frame1->set_border_width(1);
   frame1->set_shadow_type(Gtk::SHADOW_OUT);
   frame1->set_label_align(0,0.5);
   frame1->add(*hbox10);
   __options_frame->set_border_width(1);
   __options_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
   __options_frame->set_label_align(0,0.5);
   frame4->set_shadow_type(Gtk::SHADOW_NONE);
   frame4->set_label_align(0,0.5);
   __button_reset->set_size_request(60,27);
   __button_reset->set_flags(Gtk::CAN_FOCUS);
   __button_reset->set_relief(Gtk::RELIEF_NORMAL);
   __button_close->set_size_request(60,27);
   __button_close->set_flags(Gtk::CAN_FOCUS);
   __button_close->set_relief(Gtk::RELIEF_NORMAL);
   hbox11->set_border_width(6);
   hbox11->pack_start(*frame4);
   hbox11->pack_start(*__button_reset, Gtk::PACK_SHRINK, 6);
   hbox11->pack_start(*__button_close, Gtk::PACK_SHRINK, 0);
   frame2->set_border_width(1);
   frame2->set_shadow_type(Gtk::SHADOW_OUT);
   frame2->set_label_align(0,0.5);
   frame2->add(*hbox11);
   vbox2->pack_start(*frame1, Gtk::PACK_SHRINK, 0);
   vbox2->pack_start(*__options_frame);
   vbox2->pack_start(*frame2, Gtk::PACK_SHRINK, 0);
   WinOptions->set_title(_("Object Options"));
   WinOptions->set_modal(false);
   WinOptions->property_window_position().set_value(Gtk::WIN_POS_NONE);
   WinOptions->set_resizable(true);
   WinOptions->property_destroy_with_parent().set_value(false);
   WinOptions->add(*vbox2);
   __object_icon->show();
   __object_name->show();
   hbox10->show();
   frame1->show();
   __options_frame->show();
   frame4->show();
   __button_reset->show();
   __button_close->show();
   hbox11->show();
   frame2->show();
   vbox2->show();
   WinOptions->show();
   __button_close->signal_clicked().connect(sigc::mem_fun(*this, &WinOptions_glade::on_button_close_clicked));
   WinOptions->signal_delete_event().connect(sigc::mem_fun(*this, &WinOptions_glade::on_WinOptions_delete_event));
}

WinOptions_glade::~WinOptions_glade()
{  delete gmm_data;
}
