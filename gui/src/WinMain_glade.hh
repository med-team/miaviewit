/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _WINMAIN_GLADE_HH
#  define _WINMAIN_GLADE_HH


#if !defined(GLADEMM_DATA)
#define GLADEMM_DATA 
#include <gtkmm/accelgroup.h>

class GlademmData
{  
        
        Glib::RefPtr<Gtk::AccelGroup> accgrp;
public:
        
        GlademmData(Glib::RefPtr<Gtk::AccelGroup> ag) : accgrp(ag)
        {  
        }
        
        Glib::RefPtr<Gtk::AccelGroup>  getAccelGroup()
        {  return accgrp;
        }
};
#endif //GLADEMM_DATA

#include <gtkmm/window.h>
#include <gtkmm/tooltips.h>
#include <gtkmm/imagemenuitem.h>
#include <gtkmm/checkmenuitem.h>
#include <gtkmm/menubar.h>
#include <gtkmm/treeview.h>

class WinMain_glade : public Gtk::Window
{  
        
        GlademmData *gmm_data;
        Gtk::Tooltips _tooltips;
protected:
        class Gtk::ImageMenuItem *object1_add_wireframe;
        class Gtk::ImageMenuItem *object1_add_mesh;
        class Gtk::ImageMenuItem *object1_add_vectorfield;
	class Gtk::ImageMenuItem *object1_add_points; 
        class Gtk::ImageMenuItem *object1_add_tensorfield;
        class Gtk::ImageMenuItem *object1_add_texture;
        class Gtk::ImageMenuItem *object1_add_cplist;
        class Gtk::ImageMenuItem *object1_add_colormesh;
        class Gtk::ImageMenuItem *object1_add_trajectories;
        class Gtk::ImageMenuItem *object1_add_voxels;
        class Gtk::ImageMenuItem *object1_remove_selected;
        class Gtk::CheckMenuItem *view1_status;
        class Gtk::CheckMenuItem *view1_options;
        class Gtk::CheckMenuItem *view1_scene;
        class Gtk::MenuBar *menubar1;
        class Gtk::TreeView *treeview1;
        
        WinMain_glade();
        
        ~WinMain_glade();
private:
        virtual void on_open_scene_activate() = 0;
        virtual void on_save_scene_activate() = 0;
        virtual void on_snapshot_activate() = 0;
        virtual void on_quit_activate() = 0;
        virtual void add_object_wireframe() = 0;
        virtual void add_object_mesh() = 0;
        virtual void add_vectorfield() = 0;
        virtual void add_object_tensorfield() = 0;
	virtual void add_object_points() = 0;
        virtual void add_object_texture() = 0;
        virtual void add_object_cplist() = 0;
        virtual void add_object_colormesh() = 0;
        virtual void on_create_surface_vectors_activate() = 0;
        virtual void add_object_trajectories() = 0;
        virtual void add_object_voxelcloud() = 0;
        virtual void remove_object_selected() = 0;
        virtual void on_object1_activate() = 0;
        virtual void extn1_plugins_activate() = 0;
        virtual void on_about_activate() = 0;
        virtual void on_resize1_clicked() = 0;
        virtual void on_resize2_clicked() = 0;
        virtual void on_unselect_clicked() = 0;
        virtual bool on_WinMain_delete_event(GdkEventAny *ev) = 0;

	virtual void on_open_view_anterior() = 0; 
	virtual void on_open_view_posterior() = 0;
	virtual void on_open_view_left() = 0;    
	virtual void on_open_view_right() = 0;       
	virtual void on_open_view_top() = 0;    
	virtual void on_open_view_bottom() = 0;       
	virtual void on_open_view_background() = 0;       

};
#endif
