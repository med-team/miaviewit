/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#if defined __GNUC__ && __GNUC__ < 3
#error This program will crash if compiled with g++ 2.x
// see the dynamic_cast bug in the gtkmm FAQ
#endif //
#include "config.h"
/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (GETTEXT_PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif
#include "WinMain_glade.hh"
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/image.h>
#include <gtkmm/imagemenuitem.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/menu.h>
#include "../pixmaps/viewit_resize_675x900.xpm"
#include <gtkmm/box.h>
#include <gtkmm/alignment.h>
#include <gtkmm/button.h>
#include "../pixmaps/viewit_resize_1024x786.xpm"
#include "../pixmaps/viewit_unselect.xpm"
#include <gtkmm/table.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/statusbar.h>

WinMain_glade::WinMain_glade(
) : Gtk::Window(Gtk::WINDOW_TOPLEVEL)
{  
   
   Gtk::Window *WinMain = this;
   gmm_data = new GlademmData(get_accel_group());
   
   Gtk::Image *image476 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-open"), Gtk::IconSize(1)));
   Gtk::ImageMenuItem *open_scene = NULL;
   Gtk::Image *image477 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-save-as"), Gtk::IconSize(1)));
   Gtk::ImageMenuItem *save_scene = NULL;
   Gtk::Image *image478 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-save-as"), Gtk::IconSize(1)));
   Gtk::ImageMenuItem *snapshot = NULL;
   Gtk::MenuItem *separatormenuitem1 = NULL;
   Gtk::Image *image479 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-quit"), Gtk::IconSize(1)));
   Gtk::ImageMenuItem *quit = NULL;
   Gtk::Menu *file1_menu = Gtk::manage(new class Gtk::Menu());
   Gtk::MenuItem *file1 = NULL;
   Gtk::Image *image480 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_wireframe = NULL;
   Gtk::Image *image481 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_mesh = NULL;
   Gtk::Image *image482 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_vectorfield = NULL;
   object1_add_points = NULL; 
   Gtk::Image *image483 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_tensorfield = NULL;
   Gtk::Image *image484 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_texture = NULL;
   Gtk::Image *image485 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_cplist = NULL;
   Gtk::Image *image486 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   
   Gtk::Image *image_points = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_colormesh = NULL;
   Gtk::MenuItem *create_surface_vectors = NULL;
   Gtk::Image *image487 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_trajectories = NULL;
   Gtk::Image *image488 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-add"), Gtk::IconSize(1)));
   object1_add_voxels = NULL;
   Gtk::Image *image489 = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-remove"), Gtk::IconSize(1)));
   object1_remove_selected = NULL;
   Gtk::Menu *object1_menu = Gtk::manage(new class Gtk::Menu());
   Gtk::MenuItem *object1 = NULL;
   Gtk::MenuItem *extn1_plugins = NULL;
   Gtk::Menu *extn1_menu = Gtk::manage(new class Gtk::Menu());
   Gtk::MenuItem *extn1 = NULL;
   view1_status = NULL;
   view1_options = NULL;
   view1_scene = NULL;
   Gtk::Menu *view1_menu = Gtk::manage(new class Gtk::Menu());
   Gtk::MenuItem *view1 = NULL;
   Gtk::MenuItem *about = NULL;
   Gtk::Menu *help1_menu = Gtk::manage(new class Gtk::Menu());
   Gtk::MenuItem *help1 = NULL;
   menubar1 = Gtk::manage(new class Gtk::MenuBar());
   
   Glib::RefPtr<Gdk::Bitmap> _image54_mask;
   Glib::RefPtr<Gdk::Pixmap> _image54_pixmap = Gdk::Pixmap::create_from_xpm(get_default_colormap(), _image54_mask, viewit_resize_675x900_xpm);
   Gtk::Image *image54 = Gtk::manage(new class Gtk::Image(_image54_pixmap, _image54_mask));
   Gtk::HBox *hbox1 = Gtk::manage(new class Gtk::HBox(false, 2));
   Gtk::Alignment *alignment1 = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
   Gtk::Button *button1 = Gtk::manage(new class Gtk::Button());
   Glib::RefPtr<Gdk::Bitmap> _image55_mask;
   Glib::RefPtr<Gdk::Pixmap> _image55_pixmap = Gdk::Pixmap::create_from_xpm(get_default_colormap(), _image55_mask, viewit_resize_1024x786_xpm);
   Gtk::Image *image55 = Gtk::manage(new class Gtk::Image(_image55_pixmap, _image55_mask));
   Gtk::HBox *hbox2 = Gtk::manage(new class Gtk::HBox(false, 2));
   Gtk::Alignment *alignment2 = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
   Gtk::Button *button2 = Gtk::manage(new class Gtk::Button());
   Glib::RefPtr<Gdk::Bitmap> _image61_mask;
   Glib::RefPtr<Gdk::Pixmap> _image61_pixmap = Gdk::Pixmap::create_from_xpm(get_default_colormap(), _image61_mask, viewit_unselect_xpm);
   Gtk::Image *image61 = Gtk::manage(new class Gtk::Image(_image61_pixmap, _image61_mask));
   Gtk::HBox *hbox6 = Gtk::manage(new class Gtk::HBox(false, 2));
   Gtk::Alignment *alignment6 = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
   Gtk::Button *button4 = Gtk::manage(new class Gtk::Button());
   Gtk::Table *table1 = Gtk::manage(new class Gtk::Table(2, 2, false));
   treeview1 = Gtk::manage(new class Gtk::TreeView());
   
   Gtk::ScrolledWindow *scrolledwindow2 = Gtk::manage(new class Gtk::ScrolledWindow());
   Gtk::Statusbar *statusbar1 = Gtk::manage(new class Gtk::Statusbar());
   Gtk::VBox *vbox1 = Gtk::manage(new class Gtk::VBox(false, 0));

   file1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Open scene"), Gtk::AccelKey(GDK_f, Gdk::CONTROL_MASK), *image476));
   open_scene = (Gtk::ImageMenuItem *)&file1_menu->items().back();
   
   file1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Save scene"), Gtk::AccelKey(GDK_s, Gdk::CONTROL_MASK), *image477));
   save_scene = (Gtk::ImageMenuItem *)&file1_menu->items().back();
   
   file1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Snapshot"), Gtk::AccelKey(GDK_i, Gdk::CONTROL_MASK), *image478));
   snapshot = (Gtk::ImageMenuItem *)&file1_menu->items().back();
   
   file1_menu->items().push_back(Gtk::Menu_Helpers::SeparatorElem());
   separatormenuitem1 = (Gtk::MenuItem *)&file1_menu->items().back();
   
   file1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("_Quit"), Gtk::AccelKey(GDK_x, Gdk::MOD1_MASK), *image479));
   quit = (Gtk::ImageMenuItem *)&file1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Add wireframe"), Gtk::AccelKey(GDK_w, Gdk::CONTROL_MASK), *image480));
   object1_add_wireframe = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Add mesh"), Gtk::AccelKey(GDK_m, Gdk::CONTROL_MASK), *image481));
   object1_add_mesh = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Add vectorfield"), Gtk::AccelKey(GDK_v, Gdk::CONTROL_MASK), *image482));
   object1_add_vectorfield = (Gtk::ImageMenuItem *)&object1_menu->items().back();

   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Add point list"), Gtk::AccelKey(GDK_p, Gdk::CONTROL_MASK), *image_points));
   object1_add_points = (Gtk::ImageMenuItem *)&object1_menu->items().back();

   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Add tensorfield"), Gtk::AccelKey(GDK_t, Gdk::CONTROL_MASK), *image483));
   object1_add_tensorfield = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Add texture"), Gtk::AccelKey(GDK_i, Gdk::CONTROL_MASK), *image484));
   object1_add_texture = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Add cp-list"), Gtk::AccelKey(GDK_c, Gdk::CONTROL_MASK), *image485));
   object1_add_cplist = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Create colored mesh"), *image486));
   object1_add_colormesh = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Create Surface Vectors")));
   create_surface_vectors = (Gtk::MenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Create trajectories"), *image487));
   object1_add_trajectories = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Create voxels"), *image488));
   object1_add_voxels = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   object1_menu->items().push_back(Gtk::Menu_Helpers::ImageMenuElem(_("Remove object"), Gtk::AccelKey(GDK_r, Gdk::CONTROL_MASK), *image489));
   object1_remove_selected = (Gtk::ImageMenuItem *)&object1_menu->items().back();
   
   extn1_menu->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Plugins")));
   extn1_plugins = (Gtk::MenuItem *)&extn1_menu->items().back();
   
   view1_menu->items().push_back(Gtk::Menu_Helpers::CheckMenuElem(_("Status")));
   view1_status = (Gtk::CheckMenuItem *)&view1_menu->items().back();
   
   view1_menu->items().push_back(Gtk::Menu_Helpers::CheckMenuElem(_("Object Options")));
   view1_options = (Gtk::CheckMenuItem *)&view1_menu->items().back();
   
   view1_menu->items().push_back(Gtk::Menu_Helpers::CheckMenuElem(_("Scene")));
   view1_scene = (Gtk::CheckMenuItem *)&view1_menu->items().back();

   Gtk::Menu *adjust_view = Gtk::manage(new class Gtk::Menu());
   adjust_view->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Anterior"), 
							      Gtk::AccelKey(GDK_a, Gdk::CONTROL_MASK), 
							      sigc::mem_fun(*this, &WinMain_glade::on_open_view_anterior)));
   adjust_view->items().back().show();
   adjust_view->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Posterior"), Gtk::AccelKey(GDK_p, Gdk::CONTROL_MASK),
							      sigc::mem_fun(*this, &WinMain_glade::on_open_view_posterior)));
   adjust_view->items().back().show();
   adjust_view->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Left"), Gtk::AccelKey(GDK_l, Gdk::CONTROL_MASK), 
							      sigc::mem_fun(*this, &WinMain_glade::on_open_view_left)));
   adjust_view->items().back().show();
   adjust_view->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Right"), Gtk::AccelKey(GDK_r, Gdk::CONTROL_MASK), 
							      sigc::mem_fun(*this, &WinMain_glade::on_open_view_right)));
   adjust_view->items().back().show();   
   adjust_view->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Head first"), Gtk::AccelKey(GDK_t, Gdk::CONTROL_MASK), 
							      sigc::mem_fun(*this, &WinMain_glade::on_open_view_top)));   
   adjust_view->items().back().show();
   adjust_view->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Feet first"), Gtk::AccelKey(GDK_b, Gdk::CONTROL_MASK), 
							      sigc::mem_fun(*this, &WinMain_glade::on_open_view_bottom)));
   adjust_view->items().back().show();

   adjust_view->items().push_back(Gtk::Menu_Helpers::SeparatorElem());
   adjust_view->items().back().show();
   
   adjust_view->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Set Background"), Gtk::AccelKey(GDK_g, Gdk::CONTROL_MASK), 
							      sigc::mem_fun(*this, &WinMain_glade::on_open_view_background)));
   adjust_view->items().back().show();

   
   help1_menu->items().push_back(Gtk::Menu_Helpers::MenuElem(_("_About...")));
   about = (Gtk::MenuItem *)&help1_menu->items().back();
   
   menubar1->items().push_back(Gtk::Menu_Helpers::MenuElem(_("_File"), *file1_menu));
   file1 = (Gtk::MenuItem *)&menubar1->items().back();
   
   menubar1->items().push_back(Gtk::Menu_Helpers::MenuElem(_("_Object"), *object1_menu));
   object1 = (Gtk::MenuItem *)&menubar1->items().back();
   
   menubar1->items().push_back(Gtk::Menu_Helpers::MenuElem(_("Ad_just"), *adjust_view));
   Gtk::MenuItem *adj_view = (Gtk::MenuItem *)&menubar1->items().back();
   
   menubar1->items().push_back(Gtk::Menu_Helpers::MenuElem(_("_Extn"), *extn1_menu));
   extn1 = (Gtk::MenuItem *)&menubar1->items().back();
   
   menubar1->items().push_back(Gtk::Menu_Helpers::MenuElem(_("_View"), *view1_menu));
   view1 = (Gtk::MenuItem *)&menubar1->items().back();
   
   menubar1->items().push_back(Gtk::Menu_Helpers::MenuElem(_("_Help"), *help1_menu));
   help1 = (Gtk::MenuItem *)&menubar1->items().back();
   image476->set_alignment(0.5,0.5);
   image476->set_padding(0,0);
   image477->set_alignment(0.5,0.5);
   image477->set_padding(0,0);
   image478->set_alignment(0.5,0.5);
   image478->set_padding(0,0);
   image479->set_alignment(0.5,0.5);
   image479->set_padding(0,0);
   image480->set_alignment(0.5,0.5);
   image480->set_padding(0,0);
   image481->set_alignment(0.5,0.5);
   image481->set_padding(0,0);
   image482->set_alignment(0.5,0.5);
   image482->set_padding(0,0);
   image483->set_alignment(0.5,0.5);
   image483->set_padding(0,0);
   image484->set_alignment(0.5,0.5);
   image484->set_padding(0,0);
   image485->set_alignment(0.5,0.5);
   image485->set_padding(0,0);
   image486->set_alignment(0.5,0.5);
   image486->set_padding(0,0);
   image487->set_alignment(0.5,0.5);
   image487->set_padding(0,0);
   image488->set_alignment(0.5,0.5);
   image488->set_padding(0,0);
   image489->set_alignment(0.5,0.5);
   image489->set_padding(0,0);
   view1_status->set_active(false);
   view1_options->set_active(false);
   view1_scene->set_active(false);
   image54->set_alignment(0.5,0.5);
   image54->set_padding(0,0);
   hbox1->pack_start(*image54, Gtk::PACK_SHRINK, 0);
   alignment1->add(*hbox1);
   button1->set_flags(Gtk::CAN_FOCUS);
   _tooltips.set_tip(*button1, _("Resize 675x900"), "");
   button1->set_relief(Gtk::RELIEF_NORMAL);
   button1->add(*alignment1);
   image55->set_alignment(0.5,0.5);
   image55->set_padding(0,0);
   hbox2->pack_start(*image55, Gtk::PACK_SHRINK, 0);
   alignment2->add(*hbox2);
   button2->set_flags(Gtk::CAN_FOCUS);
   _tooltips.set_tip(*button2, _("Resize 1024x768"), "");
   button2->set_relief(Gtk::RELIEF_NORMAL);
   button2->add(*alignment2);
   image61->set_alignment(0.5,0.5);
   image61->set_padding(0,0);
   hbox6->pack_start(*image61, Gtk::PACK_SHRINK, 0);
   alignment6->add(*hbox6);
   button4->set_flags(Gtk::CAN_FOCUS);
   _tooltips.set_tip(*button4, _("Unselect"), "");
   button4->set_relief(Gtk::RELIEF_NORMAL);
   button4->add(*alignment6);
   table1->set_row_spacings(0);
   table1->set_col_spacings(0);
   table1->attach(*button1, 0, 1, 0, 1, Gtk::FILL, Gtk::AttachOptions(), 0, 0);
   table1->attach(*button2, 1, 2, 0, 1, Gtk::FILL, Gtk::AttachOptions(), 0, 0);
   table1->attach(*button4, 2, 3, 0, 1, Gtk::FILL, Gtk::AttachOptions(), 0, 0);
   treeview1->set_flags(Gtk::CAN_FOCUS);
   treeview1->set_headers_visible(true);
   treeview1->set_rules_hint(false);
   treeview1->set_reorderable(false);
   treeview1->set_enable_search(true);
   scrolledwindow2->set_flags(Gtk::CAN_FOCUS);
   scrolledwindow2->set_shadow_type(Gtk::SHADOW_NONE);
   scrolledwindow2->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
   scrolledwindow2->property_window_placement().set_value(Gtk::CORNER_TOP_LEFT);
   scrolledwindow2->add(*treeview1);
   vbox1->pack_start(*menubar1, Gtk::PACK_SHRINK, 0);
   vbox1->pack_start(*table1, Gtk::PACK_SHRINK, 0);
   vbox1->pack_start(*scrolledwindow2);
   vbox1->pack_start(*statusbar1, Gtk::PACK_SHRINK, 0);
   WinMain->set_title(_("ViewIT"));
   WinMain->set_modal(false);
   WinMain->property_window_position().set_value(Gtk::WIN_POS_NONE);
   WinMain->set_resizable(true);
   WinMain->property_destroy_with_parent().set_value(false);
   WinMain->add(*vbox1);
   image476->show();
   open_scene->show();
   image477->show();
   save_scene->show();
   image478->show();
   snapshot->show();
   separatormenuitem1->show();
   image479->show();
   quit->show();
   file1->show();
   image480->show();
   object1_add_wireframe->show();
   image481->show();
   object1_add_mesh->show();
   image482->show();
   object1_add_vectorfield->show();
   object1_add_points->show(); 
   image483->show();
   object1_add_tensorfield->show();
   image484->show();
   object1_add_texture->show();
   image485->show();
   object1_add_cplist->show();
   image486->show();
   object1_add_colormesh->show();
   create_surface_vectors->show();
   image487->show();
   object1_add_trajectories->show();
   image488->show();
   object1_add_voxels->show();
   image489->show();
   object1_remove_selected->show();
   object1->show();
   adj_view->show();
   extn1_plugins->show();
   extn1->show();
   view1_status->show();
   view1_options->show();
   view1_scene->show();
   view1->show();
   about->show();
   help1->show();
   menubar1->show();
   image54->show();
   hbox1->show();
   alignment1->show();
   button1->show();
   image55->show();
   hbox2->show();
   alignment2->show();
   button2->show();
   image61->show();
   hbox6->show();
   alignment6->show();
   button4->show();
   table1->show();
   treeview1->show();
   scrolledwindow2->show();
   statusbar1->show();
   vbox1->show();
   WinMain->show();
   open_scene->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::on_open_scene_activate));
   save_scene->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::on_save_scene_activate));
   snapshot->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::on_snapshot_activate));
   quit->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::on_quit_activate));
   object1_add_wireframe->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_wireframe));
   object1_add_mesh->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_mesh));
   object1_add_vectorfield->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_vectorfield));
   object1_add_points->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_points));
   object1_add_tensorfield->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_tensorfield));
   object1_add_texture->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_texture));
   object1_add_cplist->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_cplist));
   object1_add_colormesh->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_colormesh));
   create_surface_vectors->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::on_create_surface_vectors_activate));
   object1_add_trajectories->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_trajectories));
   object1_add_voxels->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::add_object_voxelcloud));
   object1_remove_selected->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::remove_object_selected));
   object1->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::on_object1_activate));
   extn1_plugins->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::extn1_plugins_activate));
   about->signal_activate().connect(sigc::mem_fun(*this, &WinMain_glade::on_about_activate));
   button1->signal_clicked().connect(sigc::mem_fun(*this, &WinMain_glade::on_resize1_clicked));
   button2->signal_clicked().connect(sigc::mem_fun(*this, &WinMain_glade::on_resize2_clicked));
   button4->signal_clicked().connect(sigc::mem_fun(*this, &WinMain_glade::on_unselect_clicked));
   WinMain->signal_delete_event().connect(sigc::mem_fun(*this, &WinMain_glade::on_WinMain_delete_event));
}

WinMain_glade::~WinMain_glade()
{  delete gmm_data;
}
