/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "options_smarties.hh"
#include <viewit/event.hh>

#include <gtkmm/image.h>
#include <gtkmm/alignment.h>
#include <gtkmm/label.h>
#include <gtkmm/frame.h>

extern CEventQueue *pending_events;

GUI_Smarties::GUI_Smarties(Gtk::VBox *box)
	: GUI_Drawable(box,true,false)
{
	add_options();
}


void GUI_Smarties::add_options() {

	Gtk::Image *increase_image = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-go-up"), Gtk::IconSize(4)));
	increase_image->set_alignment(0.5,0.5);
	increase_image->set_padding(0,0);

	Gtk::Image *decrease_image = Gtk::manage(new class Gtk::Image(Gtk::StockID("gtk-go-down"), Gtk::IconSize(4)));
	decrease_image->set_alignment(0.5,0.5);
	decrease_image->set_padding(0,0);
	

	Gtk::Label *number_incr_label = Gtk::manage(new class Gtk::Label("Number"));
	number_incr_label->set_alignment(0.5,0.5);
	number_incr_label->set_padding(0,0);
	number_incr_label->set_justify(Gtk::JUSTIFY_LEFT);
	number_incr_label->set_line_wrap(false);
	number_incr_label->set_use_markup(false);
	number_incr_label->set_selectable(false);

	Gtk::Label *number_decr_label = Gtk::manage(new class Gtk::Label("Number"));
	number_decr_label->set_alignment(0.5,0.5);
	number_decr_label->set_padding(0,0);
	number_decr_label->set_justify(Gtk::JUSTIFY_LEFT);
	number_decr_label->set_line_wrap(false);
	number_decr_label->set_use_markup(false);
	number_decr_label->set_selectable(false);

	Gtk::HBox *number_incr_hbox = Gtk::manage(new class Gtk::HBox(false, 2));
	number_incr_hbox->pack_start(*increase_image, Gtk::PACK_SHRINK, 0);
	number_incr_hbox->pack_start(*number_incr_label, Gtk::PACK_SHRINK, 0);

	Gtk::HBox *number_decr_hbox = Gtk::manage(new class Gtk::HBox(false, 2));
	number_decr_hbox->pack_start(*decrease_image, Gtk::PACK_SHRINK, 0);
	number_decr_hbox->pack_start(*number_decr_label, Gtk::PACK_SHRINK, 0);

	Gtk::Alignment *number_incr_alignment = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
	number_incr_alignment->add(*number_incr_hbox);

	Gtk::Alignment *number_decr_alignment = Gtk::manage(new class Gtk::Alignment(0.5, 0.5, 0, 0));
	number_decr_alignment->add(*number_decr_hbox);

	m_increase_smarty_number = Gtk::manage(new class Gtk::Button());
	m_increase_smarty_number->set_flags(Gtk::CAN_FOCUS);
	m_increase_smarty_number->set_relief(Gtk::RELIEF_NORMAL);
	m_increase_smarty_number->add(*number_incr_alignment);


	m_decrease_smarty_number = Gtk::manage(new class Gtk::Button());
	m_decrease_smarty_number->set_flags(Gtk::CAN_FOCUS);
	m_decrease_smarty_number->set_relief(Gtk::RELIEF_NORMAL);
	m_decrease_smarty_number->add(*number_decr_alignment);
	
	Gtk::VBox *smarty_number_vbox = Gtk::manage(new class Gtk::VBox(false, 0));
	smarty_number_vbox->set_border_width(5);
	smarty_number_vbox->pack_start(*m_increase_smarty_number, Gtk::PACK_SHRINK, 0);
	smarty_number_vbox->pack_start(*m_decrease_smarty_number, Gtk::PACK_SHRINK, 0);
	
	Gtk::Label *smarties_frame_label = Gtk::manage(new class Gtk::Label("Tensorfield propperties"));
	smarties_frame_label->set_alignment(0.5,0.5);
	smarties_frame_label->set_padding(0,0);
	smarties_frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	smarties_frame_label->set_line_wrap(false);
	smarties_frame_label->set_use_markup(false);
	smarties_frame_label->set_selectable(false);
   
	Gtk::Frame *smarties_frame = Gtk::manage(new class Gtk::Frame());
	smarties_frame->set_border_width(10);
	smarties_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	smarties_frame->set_label_align(0,0.5);
	smarties_frame->add(*smarty_number_vbox);
	smarties_frame->set_label_widget(*smarties_frame_label);

	increase_image->show();
	decrease_image->show();
	number_incr_alignment->show();
	number_decr_alignment->show();
	number_incr_label->show();
	number_decr_label->show();
	number_incr_hbox->show();
	number_decr_hbox->show();
	m_increase_smarty_number->show();
	m_decrease_smarty_number->show();
	smarty_number_vbox->show();	
	smarties_frame_label->show();
	smarties_frame->show();

	this->pack_start(*smarties_frame);
	
	m_increase_smarty_number->signal_clicked().connect(sigc::mem_fun(*this, &GUI_Smarties::on_m_increase_smarty_number_clicked));   
	m_decrease_smarty_number->signal_clicked().connect(sigc::mem_fun(*this, &GUI_Smarties::on_m_decrease_smarty_number_clicked));   
}


void GUI_Smarties::on_m_increase_smarty_number_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_SMARTIES_INCREASE_SMARTY_NUMBER));
	pending_events->push(event);
}

void GUI_Smarties::on_m_decrease_smarty_number_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_SMARTIES_DECREASE_SMARTY_NUMBER));
	pending_events->push(event);
}


void GUI_Smarties::reset()
{
	GUI_Drawable::reset();
}
