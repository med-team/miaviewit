/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include <progress_window.hh>
#include <gtkmm/window.h>
#include <gtkmm/progressbar.h>
#include <glibmm/main.h>

class  CLoadProgressImpl: public Gtk::Window {
public:	
	CLoadProgressImpl();
	
	void update(int step);
	void set_range(int range); 
	void pulse();

private:	
	Gtk::ProgressBar the_bar;
	Glib::RefPtr<Glib::MainContext> the_context; 
	int m_range; 
};

CLoadProgressImpl::CLoadProgressImpl():
	Gtk::Window(Gtk::WINDOW_TOPLEVEL),
	the_context(Glib::MainContext::get_default()), 
	m_range(0)
{
	the_bar.set_fraction(0.0);
	the_bar.show();
	add(the_bar);
	
	set_title("Loading ...");
	set_keep_above(true);
	set_type_hint(Gdk::WINDOW_TYPE_HINT_DIALOG);
}

void CLoadProgressImpl::update(int step)
{
	the_bar.set_fraction(double(step)/double(m_range));
	the_context->iteration(false);
}


void CLoadProgressImpl::set_range(int range)
{
	m_range = range; 
}

void CLoadProgressImpl::pulse()
{
	the_bar.pulse();
	the_context->iteration(false);
}



CLoadProgress::CLoadProgress():
	impl(new CLoadProgressImpl())
{
	impl->show();
}

CLoadProgress::~CLoadProgress()
{
	impl->hide();
	delete impl;
}

void CLoadProgress::do_update(int step)
{
	impl->update(step); 
}
void CLoadProgress::do_set_range(int range)
{
	impl->set_range(range); 
}

void CLoadProgress::do_pulse()
{
	impl->pulse(); 
}
	
