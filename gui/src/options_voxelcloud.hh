/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPTIONS_VOXELCLOUD_HH
#define OPTIONS_VOXELCLOUD_HH

#include "options_drawable.hh"

#include <gtkmm/spinbutton.h>
#include <gtkmm/button.h>

class GUI_VoxelCloud : public GUI_Drawable {
private:
        class Gtk::Button *m_diffusion;
        class Gtk::SpinButton *m_lifetime;
	
	void on_m_lifetime_changed();
	void on_m_diffusion_clicked();
	
	void add_options(float lifetime);

public:
	GUI_VoxelCloud(Gtk::VBox *box);
	virtual void reset();
};

#endif
