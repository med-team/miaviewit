/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _WINMAIN_HH
#  include "WinMain_glade.hh"
#  define _WINMAIN_HH

#include <gtkmm/liststore.h>
#include "SignalHandler.hh"

#include <mia.hh>

#include <viewit/glviewer.hh>
#include <viewit/snapshot.hh>

///////////////////////////////////////////////////////////////////////////////
//
// class WinMain - declaration
//
///////////////////////////////////////////////////////////////////////////////

class TDrawable;

class WinMain : public WinMain_glade
{  
	PGLViewer m_glview; 
public:
	WinMain(PGLViewer glview);
	~WinMain();

	// updates the CheckMenuItem status of menu->view->..., whenever a Window is about to be hidden
	void on_WinMain_hide_notification(std::string name);	
	
	// updates the object treeview
	void on_WinMain_objectmap_changed();	

	void add_vectorfield();
	void add_object_trajectories();
	void add_object_voxelcloud();
	void add_object_mesh();
	void add_object_colormesh();
	void add_object_texture();
	void add_object_cplist();
	void add_object_wireframe();
	void add_object_tensorfield();
	void on_create_surface_vectors_activate();
	void remove_object_selected();

	std::string add_object_mesh(const std::string& filename, const std::string& name, bool active);
	std::string add_vectorfield(const std::string& filename, const std::string& name);
	void add_object_points(); 
	std::string add_object_points(const std::string& filename, const std::string& name, bool visible);
		
protected:        
	
	//Treeview model columns:
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns()
		{ add(m_col_type); add(m_col_name); add(m_col_visible); add(m_col_object); add(m_col_has_visibility);}

		Gtk::TreeModelColumn<std::string> m_col_type;			// holds the type of the object and shows the corresponding icon
		Gtk::TreeModelColumn<std::string> m_col_name;			// name of the object
		Gtk::TreeModelColumn<bool> m_col_visible;			// visibility
		// hidden columns
		Gtk::TreeModelColumn<bool> m_col_has_visibility;
		Gtk::TreeModelColumn<TDrawable *> m_col_object;			// (hidden) pointer to the associated Object
	};
	ModelColumns m_Columns;
	Glib::RefPtr<Gtk::ListStore> m_refTreeModel;	
	
	CSnapshotClient *		m_snap_server;
	std::list<std::string> *	m_plugin_search_paths;
	

	// object list
	void Icon_SlotCell_data(Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator& iter);	
	void Visible_SlotCell_data(Gtk::CellRenderer *cell, const Gtk::TreeModel::iterator& iter);	
	void add_columns();
	void add_object_to_list(TDrawable *object, const std::string type, const std::string name, const bool visible);
	void remove_object_from_list(const std::string &name, bool remove_from_viewer);

	void on_object_visibility_toggled(const Glib::ustring& path_string);
	void on_object_row_selected();

	
	// Icons to be displayed in treeview1
	typedef std::map<std::string, Glib::RefPtr<Gdk::Pixbuf> > IconMapType;
	IconMapType Icons;
	Glib::RefPtr<Gdk::Pixbuf> Icon_Unknown;

	
	void init_menu();
	void init_plugins();
	
	void on_view1_menu_element_activated(Gtk::CheckMenuItem *sender, std::string name);

	
	
	// file menu
	void on_open_scene_activate();
        void on_save_scene_activate();
        void on_snapshot_activate();
	void on_quit_activate();
        void on_ext_clicked();

	// adjust view menu
	void on_open_view_anterior();
	void on_open_view_posterior();
	void on_open_view_left();    
	void on_open_view_right();
	void on_open_view_top();
	void on_open_view_bottom();
	void on_open_view_background();

	
	// help menu
 	void on_about_activate();

	// object menu 
	void on_object1_activate();

	// extensions menu
        void extn1_plugins_activate();
	
	// toolbar
	void on_resize1_clicked();
        void on_resize2_clicked();
	void on_unselect_clicked();

	// window events
	bool on_WinMain_delete_event(GdkEventAny *ev);
	bool on_WinMain_expose_event(GdkEventExpose *ev);
        void on_WinMain_realize();
};
#endif
