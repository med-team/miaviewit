/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "vectorfieldvbox.hh"

#include <gtkmm/frame.h>
#include <gtkmm/label.h>

#include <viewit/event.hh>

TVectorfieldVBox::TVectorfieldVBox()
{
	m_trajectories = Gtk::manage(new class Gtk::Button("Create trajectories"));
	m_trajectories->set_flags(Gtk::CAN_FOCUS);
	m_trajectories->set_relief(Gtk::RELIEF_NORMAL);

	m_pixelcloud = Gtk::manage(new class Gtk::Button("Create pixelcloud"));
	m_pixelcloud->set_flags(Gtk::CAN_FOCUS);
	m_pixelcloud->set_relief(Gtk::RELIEF_NORMAL);
	
	Gtk::HBox *button_hbox = Gtk::manage(new class Gtk::HBox(false, 0));
	button_hbox->set_border_width(7);
	button_hbox->pack_start(*m_trajectories, Gtk::PACK_EXPAND_PADDING, 0);
	button_hbox->pack_start(*m_pixelcloud, Gtk::PACK_EXPAND_PADDING, 6);

	Gtk::Label *vectorfield_frame_label = Gtk::manage(new class Gtk::Label("Vectorfield options"));
	vectorfield_frame_label->set_alignment(0.5,0.5);
	vectorfield_frame_label->set_padding(0,0);
	vectorfield_frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	vectorfield_frame_label->set_line_wrap(false);
	vectorfield_frame_label->set_use_markup(false);
	vectorfield_frame_label->set_selectable(false);

	Gtk::Frame *vectorfield_frame = Gtk::manage(new class Gtk::Frame());
	vectorfield_frame->set_border_width(10);
	vectorfield_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	vectorfield_frame->set_label_align(0,0.5);
	vectorfield_frame->add(*button_hbox);
	vectorfield_frame->set_label_widget(*vectorfield_frame_label);

	m_trajectories->show();
	m_pixelcloud->show();
	button_hbox->show();
	vectorfield_frame_label->show();
	vectorfield_frame->show();

	this->pack_start(*vectorfield_frame);
	
	m_trajectories->signal_clicked().connect(sigc::mem_fun(*this,&TVectorfieldVBox::on_m_trajectories_clicked));
	m_pixelcloud->signal_clicked().connect(sigc::mem_fun(*this,&TVectorfieldVBox::on_m_pixelcloud_clicked));
}


void TVectorfieldVBox::on_m_trajectories_clicked()
{
	
}

void TVectorfieldVBox::on_m_pixelcloud_clicked()
{

}
