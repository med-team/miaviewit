/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __USERINI_HH
#define __USERINI_HH

#include <string>


namespace xmlpp {
	class DomParser;
}

class XMLini {
	
private:
	xmlpp::DomParser *parser;
	

public:
	
	XMLini();
	~XMLini();

	bool write_to_file(std::string &fn);
	bool read_from_file(std::string &fn);
	
	void set_value(std::string const &section, std::string const &key, std::string const &value);
	bool get_value(std::string &section, std::string &key, std::string &value) const;
	
	bool remove_section(const std::string &section);
};

#endif
