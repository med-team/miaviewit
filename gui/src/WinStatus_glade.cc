/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#if defined __GNUC__ && __GNUC__ < 3
#error This program will crash if compiled with g++ 2.x
// see the dynamic_cast bug in the gtkmm FAQ
#endif //
#include "config.h"
/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (GETTEXT_PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif
#include "WinStatus_glade.hh"
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/scrolledwindow.h>

WinStatus_glade::WinStatus_glade(
) : Gtk::Window(Gtk::WINDOW_TOPLEVEL)
{  
   
   Gtk::Window *WinStatus = this;
   gmm_data = new GlademmData(get_accel_group());
   status = Gtk::manage(new class Gtk::TextView());
   
   Gtk::ScrolledWindow *scrolledwindow1 = Gtk::manage(new class Gtk::ScrolledWindow());
   status->set_flags(Gtk::CAN_FOCUS);
   status->set_editable(false);
   status->set_cursor_visible(false);
   status->set_pixels_above_lines(0);
   status->set_pixels_below_lines(0);
   status->set_pixels_inside_wrap(0);
   status->set_left_margin(0);
   status->set_right_margin(0);
   status->set_indent(0);
   status->set_wrap_mode(Gtk::WRAP_NONE);
   status->set_justification(Gtk::JUSTIFY_LEFT);
   scrolledwindow1->set_flags(Gtk::CAN_FOCUS);
   scrolledwindow1->set_shadow_type(Gtk::SHADOW_NONE);
   scrolledwindow1->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
   scrolledwindow1->property_window_placement().set_value(Gtk::CORNER_TOP_LEFT);
   scrolledwindow1->add(*status);
   WinStatus->set_size_request(400,200);
   WinStatus->set_title(_("Status"));
   WinStatus->set_modal(false);
   WinStatus->property_window_position().set_value(Gtk::WIN_POS_NONE);
   WinStatus->set_resizable(true);
   WinStatus->property_destroy_with_parent().set_value(false);
   WinStatus->add(*scrolledwindow1);
   status->show();
   scrolledwindow1->show();
   WinStatus->show();
   WinStatus->signal_delete_event().connect(sigc::mem_fun(*this, &WinStatus_glade::on_WinStatus_delete_event));
}

WinStatus_glade::~WinStatus_glade()
{  delete gmm_data;
}
