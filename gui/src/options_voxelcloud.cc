/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "options_voxelcloud.hh"
#include <viewit/event.hh>

#include <gtkmm/adjustment.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>
#include <gtkmm/frame.h>

extern CEventQueue *pending_events;

GUI_VoxelCloud::GUI_VoxelCloud(Gtk::VBox *box) 
	: GUI_Drawable(box,true,false)
{
	event_t event;
	event.what = event_t::ev_command; 
	TVoxelCloudEventInfo *info = new TVoxelCloudEventInfo(EV_VOXELCLOUD_GET_STATE);
	event.info.reset(info);
	pending_events->push(event);
	pending_events->handle_events();
	add_options(info->get_lifetime());
}
	
void GUI_VoxelCloud::add_options(float lifetime) 
{
	m_diffusion = Gtk::manage(new class Gtk::Button("Diffusion"));
	m_diffusion->set_size_request(-1,27);
	m_diffusion->set_flags(Gtk::CAN_FOCUS);
	m_diffusion->set_relief(Gtk::RELIEF_NORMAL);
   
	Gtk::Label *lifetime_label = Gtk::manage(new class Gtk::Label("Number of cycles"));
	lifetime_label->set_alignment(0.5,0.5);
	lifetime_label->set_padding(0,0);
	lifetime_label->set_justify(Gtk::JUSTIFY_LEFT);
	lifetime_label->set_line_wrap(false);
	lifetime_label->set_use_markup(false);
	lifetime_label->set_selectable(false);

	Gtk::Adjustment *lifetime_adj = Gtk::manage(new class Gtk::Adjustment(lifetime, 2000, 10000, 100, 1000, 1000));
	
	m_lifetime = Gtk::manage(new class Gtk::SpinButton(*lifetime_adj, 1, 0));
	m_lifetime->set_flags(Gtk::CAN_FOCUS);
	m_lifetime->set_update_policy(Gtk::UPDATE_IF_VALID);
	m_lifetime->set_numeric(true);
	m_lifetime->set_digits(0);
	m_lifetime->set_wrap(false);
	
	Gtk::HBox *lifetime_hbox = Gtk::manage(new class Gtk::HBox(false, 0));
	lifetime_hbox->pack_start(*lifetime_label, Gtk::PACK_SHRINK, 5);
	lifetime_hbox->pack_start(*m_lifetime);

	Gtk::HBox *voxelcloud_hbox = Gtk::manage(new class Gtk::HBox(false, 0));
	voxelcloud_hbox->set_border_width(10);
	voxelcloud_hbox->pack_start(*m_diffusion, Gtk::PACK_EXPAND_WIDGET, 32);
	voxelcloud_hbox->pack_start(*lifetime_hbox);

	Gtk::Label *voxelcloud_frame_label = Gtk::manage(new class Gtk::Label("Voxel propperties"));
	voxelcloud_frame_label->set_alignment(0.5,0.5);
	voxelcloud_frame_label->set_padding(0,0);
	voxelcloud_frame_label->set_justify(Gtk::JUSTIFY_LEFT);
	voxelcloud_frame_label->set_line_wrap(false);
	voxelcloud_frame_label->set_use_markup(false);
	voxelcloud_frame_label->set_selectable(false);

	Gtk::Frame *voxelcloud_frame = Gtk::manage(new class Gtk::Frame());
	voxelcloud_frame->set_border_width(10);
	voxelcloud_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	voxelcloud_frame->set_label_align(0,0.5);
	voxelcloud_frame->add(*voxelcloud_hbox);
	voxelcloud_frame->set_label_widget(*voxelcloud_frame_label);
	
	m_diffusion->show();
	lifetime_label->show();
	m_lifetime->show();
	lifetime_hbox->show();
	voxelcloud_hbox->show();
	voxelcloud_frame_label->show();
	voxelcloud_frame->show();
	
	this->pack_start(*voxelcloud_frame);
	
	m_lifetime->signal_value_changed().connect(sigc::mem_fun(*this, &GUI_VoxelCloud::on_m_lifetime_changed));
	m_diffusion->signal_clicked().connect(sigc::mem_fun(*this,&GUI_VoxelCloud::on_m_diffusion_clicked));
}

void GUI_VoxelCloud::on_m_lifetime_changed()
{
	event_t event;
	event.what = event_t::ev_command; 
	TVoxelCloudEventInfo *info = new TVoxelCloudEventInfo(EV_VOXELCLOUD_SET_LIFETIME);
	info->set_lifetime(m_lifetime->get_value());
	event.info.reset(info);
	pending_events->push(event);
}

void GUI_VoxelCloud::on_m_diffusion_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_VOXELCLOUD_TOGGLE_DIFFUSION));
	pending_events->push(event);
}

void GUI_VoxelCloud::reset()
{
	GUI_Drawable::reset();
	
	if (m_lifetime) {
		m_lifetime->set_value(2000.0);
	}
}
