/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPTIONS_SMARTIES_HH
#define OPTIONS_SMARTIES_HH

#include "options_drawable.hh"

class GUI_Smarties : public GUI_Drawable {
private:
	class Gtk::Button *	 m_increase_smarty_number;
	class Gtk::Button *	 m_decrease_smarty_number;
	
	void add_options();
	
	void on_m_increase_smarty_number_clicked();
	void on_m_decrease_smarty_number_clicked();	
	
public:
	GUI_Smarties(Gtk::VBox *box);
	virtual void reset();	
};

#endif
