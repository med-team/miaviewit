/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPTIONS_MESH_HH
#define OPTIONS_MESH_HH

#include "options_drawable.hh"

#include <gtkmm/checkbutton.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/drawingarea.h>

#include <viewit/event.hh>

///////////////////////////////////////////////////////////////////////////////
//
// Helper Class to show a color rectangle
//
///////////////////////////////////////////////////////////////////////////////

class ColorRectangle : public Gtk::DrawingArea
{
public:
	ColorRectangle(int x_size, int y_size, Gdk::Color col);
	bool on_expose_event(GdkEventExpose* event);
	void set_color(Gdk::Color col);
	Gdk::Color get_color() const;
	
  
protected:
	int m_width, m_height;
	Gdk::Color m_color;
};



///////////////////////////////////////////////////////////////////////////////
//
// Class GUI_Mesh
//
///////////////////////////////////////////////////////////////////////////////


class GUI_Mesh : public GUI_Drawable {
private:
	// mesh options
	class Gtk::ToggleButton *	m_culled;
        class Gtk::ToggleButton *	m_glossy;
	class Gtk::ToggleButton *	m_lightmodel;
        class Gtk::RadioButton *	m_culled_front;
        class Gtk::RadioButton *	m_culled_back;

	// color options
        class ColorRectangle *m_color1_area;
        class ColorRectangle *m_color2_area;
        class ColorRectangle *m_color3_area;
	class Gtk::Button    *m_color_scale;
	class Gtk::Button    *m_color_legend;	
	
	
	void add_mesh_options(PEventInfo info);
	void add_color_options(PEventInfo info);
	
	// mesh options
	void on_m_culled_toggled();
	void on_m_glossy_toggled();
	void on_m_lightmodel_toggled();
	void on_m_culled_front_toggled();
	
	// color options
	bool on_color1_button_pressed(GdkEventButton* event);
	bool on_color2_button_pressed(GdkEventButton* event);
	bool on_color3_button_pressed(GdkEventButton* event);
	void on_m_color_scale_clicked();
	void on_m_color_legend_clicked();

public:
	GUI_Mesh(Gtk::VBox *box);
	
	virtual void reset();
};
#endif
