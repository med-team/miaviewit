/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"
#include "SignalHandler.hh"

SignalHandler::SignalHandler()
{;}


SignalHandler::~SignalHandler() throw()
{;}

///////////////////////////////////////////////////////////////////////////////
//
// signals to status window
//
///////////////////////////////////////////////////////////////////////////////

SignalHandler::type_signal_WinStatus_set_visibility SignalHandler::WinStatus_set_visibility()
{
	return m_signal_WinStatus_set_visibility;
}


SignalHandler::type_signal_WinStatus_append_text SignalHandler::WinStatus_append_text()
{
	return m_signal_WinStatus_append_text;
}


///////////////////////////////////////////////////////////////////////////////
//
// signals to scene window
//
///////////////////////////////////////////////////////////////////////////////


SignalHandler::type_signal_WinScene_detach SignalHandler::WinScene_detach()
{
	return m_signal_WinScene_detach;
}


SignalHandler::type_signal_WinScene_resize SignalHandler::WinScene_resize()
{
	return m_signal_WinScene_resize;
}


SignalHandler::type_signal_WinScene_press_key SignalHandler::WinScene_press_key()
{
	return m_signal_WinScene_press_key;
}


SignalHandler::type_signal_WinScene_set_visibility SignalHandler::WinScene_set_visibility()
{
	return m_signal_WinScene_set_visibility;
}


///////////////////////////////////////////////////////////////////////////////
//
// signals to main window
//
///////////////////////////////////////////////////////////////////////////////


SignalHandler::type_signal_WinMain_hide_notification SignalHandler::WinMain_hide_notification()
{
	return m_signal_WinMain_hide_notification;
}


SignalHandler::type_signal_WinMain_objectmap_changed SignalHandler::WinMain_objectmap_changed() const
{
	return m_signal_WinMain_objectmap_changed;
}


///////////////////////////////////////////////////////////////////////////////
//
// signals to options window
//
///////////////////////////////////////////////////////////////////////////////

SignalHandler::type_signal_WinOptions_set_visibility SignalHandler::WinOptions_set_visibility()
{
	return m_signal_WinOptions_set_visibility;
}

SignalHandler::type_signal_WinOptions_set_object SignalHandler::WinOptions_set_object()
{
	return m_signal_WinOptions_set_object;
}

SignalHandler::type_signal_WinOptions_set_vectorfield SignalHandler::WinOptions_set_vectorfield()
{
	return m_signal_WinOptions_set_vectorfield;
}

SignalHandler::type_signal_WinOptions_set_viewer SignalHandler::WinOptions_set_viewer()
{
	return m_signal_WinOptions_set_viewer;
}

///////////////////////////////////////////////////////////////////////////////
//
// signals to options window
//
///////////////////////////////////////////////////////////////////////////////

void SignalHandler::show_message(const std::string& message)
{
	std::string msg = message;
	m_signal_WinStatus_append_text.emit(msg);
}
