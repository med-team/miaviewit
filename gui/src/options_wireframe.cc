/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "options_wireframe.hh"
#include <viewit/event.hh>

#include <gtkmm/frame.h>
#include <gtkmm/label.h>

extern CEventQueue *pending_events;

GUI_Wireframe::GUI_Wireframe(Gtk::VBox *box) 
	: GUI_Drawable(box)
{
	add_options();
}



void GUI_Wireframe::add_options()
{
	m_toggle_parts = Gtk::manage(new class Gtk::Button("Partial view"));
	m_toggle_parts->set_flags(Gtk::CAN_FOCUS);
	m_toggle_parts->set_border_width(6);
	m_toggle_parts->set_relief(Gtk::RELIEF_NORMAL);

	Gtk::Label *wireframe_label = Gtk::manage(new class Gtk::Label("Wireframe propperties"));
	wireframe_label->set_alignment(0.5,0.5);
	wireframe_label->set_padding(0,0);
	wireframe_label->set_justify(Gtk::JUSTIFY_LEFT);
	wireframe_label->set_line_wrap(false);
	wireframe_label->set_use_markup(false);
	wireframe_label->set_selectable(false);

	
	Gtk::Frame *wireframe_frame = Gtk::manage(new class Gtk::Frame());
	wireframe_frame->set_border_width(10);
	wireframe_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	wireframe_frame->set_label_align(0,0.5);
	wireframe_frame->add(*m_toggle_parts);
	wireframe_frame->set_label_widget(*wireframe_label);
	
	m_toggle_parts->show();
	wireframe_label->show();
	wireframe_frame->show();
	this->pack_start(*wireframe_frame);
	
	m_toggle_parts->signal_clicked().connect(sigc::mem_fun(*this,&GUI_Wireframe::on_m_toggle_parts_clicked));
}
     
void GUI_Wireframe::on_m_toggle_parts_clicked()
{
	event_t event;
	event.what = event_t::ev_command; 
	event.info = PEventInfo(new CEventInfo(EV_WIREFRAME_TOGGLE_VIEW));
	pending_events->push(event);
}

void GUI_Wireframe::reset()
{
	GUI_Drawable::reset();
}
