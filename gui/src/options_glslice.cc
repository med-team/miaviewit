/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#include "options_glslice.hh"
#include <viewit/glimage.hh>

#include <gtkmm/adjustment.h>
#include <gtkmm/frame.h>
#include <gtkmm/label.h>

extern CEventQueue *pending_events;

GUI_GLSlice::GUI_GLSlice(Gtk::VBox *box) 
	: GUI_Slice(box), m_translucency(NULL)
{
	event_t event;
	event.what = event_t::ev_command; 
	TGLSliceEventInfo * info = new TGLSliceEventInfo(EV_GLSLICE_GET_STATE);
	event.info.reset(info);
	pending_events->push(event);
	pending_events->handle_events();

	add_translucency_options(info->get_translucency());
}
	
void GUI_GLSlice::add_translucency_options(float translucency)
{
	Gtk::Adjustment *translucency_adj = Gtk::manage(new class Gtk::Adjustment(translucency, 0.0, 1.0, 0.05, 0.1, 0.0));
	m_translucency = Gtk::manage(new class Gtk::HScale(*translucency_adj));
	m_translucency->set_flags(Gtk::CAN_FOCUS);
	m_translucency->set_update_policy(Gtk::UPDATE_CONTINUOUS);
	m_translucency->set_digits(2);
	m_translucency->set_draw_value(true);
	m_translucency->set_value_pos(Gtk::POS_TOP);
   
	Gtk::Label *translucency_label = Gtk::manage(new class Gtk::Label("Translucency"));
	translucency_label->set_alignment(0.5,0.5);
	translucency_label->set_padding(0,0);
	translucency_label->set_justify(Gtk::JUSTIFY_LEFT);
	translucency_label->set_line_wrap(false);
	translucency_label->set_use_markup(false);
	translucency_label->set_selectable(false);
	
	Gtk::Frame *translucency_frame = Gtk::manage(new class Gtk::Frame());
	translucency_frame->set_border_width(10);
	translucency_frame->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
	translucency_frame->set_label_align(0,0.5);
	translucency_frame->add(*m_translucency);
	translucency_frame->set_label_widget(*translucency_label);
	
	m_translucency->show();
	translucency_label->show();
	translucency_frame->show();
	this->pack_start(*translucency_frame);
	
	m_translucency->signal_value_changed().connect(sigc::mem_fun(*this, &GUI_GLSlice::on_m_translucency_value_changed));
}

void GUI_GLSlice::on_m_translucency_value_changed()
{
	event_t event;
	event.what = event_t::ev_command; 
	TGLSliceEventInfo *info = new TGLSliceEventInfo(EV_GLSLICE_SET_TRANSLUCENCY);
	info->set_translucency(m_translucency->get_value());
	event.info.reset(info);
	pending_events->push(event);
}

void GUI_GLSlice::reset()
{
	GUI_Slice::reset();
	if (m_translucency) m_translucency->set_value(0.2);
}
