/* -*- mia-c++ -*-
 *
 * This file is part of viewitgui - a library and program for the
 * visualization of 3D data sets. 
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Mirco Hellmann, Gert Wollny
 *
 * viewitgui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * viewitgui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with viewitgui; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef VECTORFIELDVBOX_HH
#define VECTORFIELDVBOX_HH

#include <gtkmm/box.h>
#include <gtkmm/button.h>

class TVectorfieldVBox : public Gtk::VBox {

	void on_m_trajectories_clicked();
	void on_m_pixelcloud_clicked();
	
protected:
	Gtk::Button *		m_trajectories;
	Gtk::Button *		m_pixelcloud;

	
public:
	TVectorfieldVBox();
};

#endif
